package com.dlsuuniweek.ahro;

import android.app.Application;
import android.content.pm.PackageManager;

import com.facebook.react.ReactApplication;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;

import org.reactnative.camera.RNCameraPackage;

import java.util.Arrays;
import java.util.List;
//
//import io.invertase.firebase.auth.RNFirebaseAuthPackage;
//import io.invertase.firebase.database.RNFirebaseDatabasePackage;

//import com.airbnb.android.react.lottie.LottiePackage;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new VectorIconsPackage(),
            new RNCameraPackage(),
            new AsyncStoragePackage(),
//            new LottiePackage(),
            new ReactNativeOneSignalPackage()
//              new RNFirebaseDatabasePackage(),
//              new RNFirebaseAuthPackage(),
//              new FBSDKPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
//    if(isFirstInstall())
//      MoEHelper.getInstance(getApplicationContext()).setExistingUser(true);
//    else
//      MoEHelper.getInstance(getApplicationContext()).setExistingUser(false);
//
//    MoEngage moEngage =
//            new MoEngage.Builder(this, "RKAIE2R4ZFNEVFQPZGCBDVAU")
//                    .setLogLevel(Logger.VERBOSE)
//                    .build();
//    MoEngage.initialise(moEngage);
  }

  public boolean isFirstInstall() {
    try {
      long firstInstallTime =   getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).firstInstallTime;
      long lastUpdateTime = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).lastUpdateTime;
      return firstInstallTime == lastUpdateTime;
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
      return true;
    }
  }

}
