import React from 'react';
import {Text, View, StyleSheet} from "react-native";
import {isEmpty, validate} from "./validator";
import PropTypes from 'prop-types'
import {Input, Button, Icon} from "react-native-elements";


export default class Form extends React.Component {

    static FIELD_FIRST_NAME         = "first name";
    static FIELD_LAST_NAME          = "last name";
    static FIELD_FULL_NAME          = "full name"
    static FIELD_USERNAME           = "username";
    static FIELD_EMAIL              = 'email';
    static FIELD_PHONE_NUMBER       = "phone number";
    static FIELD_PASSWORD           = 'password';
    static FIELD_CONFIRM_PASSWORD   = "confirm password";

    static TYPE_RADIO_GROUP         = 'radio group';
    static TYPE_TEXT                = 'text';
    static TYPE_NUMBER              = 'number';
    static TYPE_SWITCH              = 'switch';
    static TYPE_DATE                = 'date'

    constructor(props) {
        super(props);

        this.state = this.createFormState();
        console.log(this.state);

        //bind functions
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    createFormState(fields, error) {
        const state     = {};
        state.formData  = {};
        state.fields    = [];

        let counter = 0;
        this.props.fields.forEach((field) => {
            let {key, type, value}  = field;
            // let fieldKey            = (key == null) ? type + counter++ : key;
            let fieldKey = type  // <--- change this later TODO: form generator. how to map field keys to database keys
            state.formData[fieldKey]= {type: type, value: value};
            state.fields.push({key: fieldKey, ...field})
        });

        state.error = {};
        state.error.general = (error)?error.toString():" ";//TODO: bug: needs a space or error will show
        state.initialKey = "Iwillneverbeequaltopropsunlessyoucopyme";

        console.log("==createformstate");
        console.log(state);
        console.log("createformstatejnj==");
        return state;
    }

    onChange(key, text) {
        const state = this.state.formData;
        state[key]['value'] = text;
        this.setState(state);
    }

    onSubmit() {
        const data = this.state.formData;
        console.log(data);
        const result = validate(data);

        console.log("validate result:");
        console.log(result);
        this.setState({error: result.error});

        if (!result.success){
            console.log("error. setting state");
            this.setState({error: result.error});
        }
        else {
            if (!(this.props.uniqueKey == null)) this.setState({initialKey:this.props.uniqueKey});
            this.props.onSubmitForm(this.extractData(data))
        }
    }

    extractData(data) {
        const retData = {};

        Object.keys(data).forEach((key) => {
                retData[key] = data[key].value;
        });

        return retData;
    }

    //append error props to componentShouldUpdate


    render(){
        let errors = Object.values(this.state.error).reverse();
        let index = errors.length;
        while (index-- && !errors[index]);
        let firsterror =(index >= 0) ? errors[index] : undefined;

        let {showLabel, collapseValidationError, highlightError, selectionColor,
            style, fieldStyle, inputContainerStyle, inputStyle, inputContentStyle} = this.props;

        return(
            <View style={[styles.wrapper,style]}>
                <View style={[styles.wrapper1, fieldStyle]}>
                {
                    this.state.fields.map((data) => {
                        let {key, type, label, placeholder, autoFocus, secureTextEntry, defaultValue, customBackgroundColor} = data;
                        // console.log(data)
                        // console.log(this.state.error[key])
                        return (
                            <Input
                                key             = {key}
                                label           = {(showLabel)?label:null}
                                placeholder     = {placeholder||label||type}
                                placeholderTextColor = {this.props.placeholderTextColor}
                                autoFocus       = {autoFocus}
                                onChangeText    = {(text) => this.onChange(key, text)}
                                secureTextEntry = {secureTextEntry}
                                defaultValue    = {defaultValue}
                                errorMessage    = {(!collapseValidationError)? this.state.error[key] : null}
                                rightIcon       = {(this.state.error[key])&& highlightError &&
                                                    <Icon name='error' size={18}
                                                        color='#DC4E41' containerStyle={{marginRight:5,}}/>}
                                selectionColor  = {selectionColor || 'rgba(200,20,20,0.5)'}

                                containerStyle={[inputContainerStyle]}
                                inputContainerStyle={[
                                    {backgroundColor: customBackgroundColor || "#ebebeb"},
                                     inputStyle]}
                                inputStyle={[inputContentStyle]}
                            />
                        )
                    })
                }
                </View>
                {
                    (collapseValidationError  && firsterror && <Text style={[styles.errorText]}>{firsterror}</Text>) ||
                    (!isEmpty(this.props.error)) &&
                    <Text style={[styles.errorText]}>
                        {this.props.error}
                    </Text> ||
                    <Text style={[styles.errorText]}> </Text>
                }
                <Button
                    raised
                    title           = {this.props.buttonTitle}
                    borderRadius    = {4}
                    containerStyle  = {[styles.containerView,this.props.buttonContainerStyle]}
                    buttonStyle     = {[styles.button,this.props.buttonStyle]}
                    titleStyle      = {[styles.buttonText,this.props.buttonTextStyle]}
                    onPress         = {this.onSubmit}
                    loading         = {(this.state.initialKey === this.props.uniqueKey)}
                />
            </View>
        );
    }
}







Form.propTypes = {
    // fields           : PropTypes.object.isRequired,
    buttonTitle         : PropTypes.string.isRequired,
    // onSubmitForm     : PropTypes.func.isRequired,
    // showLabel           : PropTypes.bool,
    // buttonStyle         : PropTypes.any.optional,
    // buttonTextStyle     : PropTypes.any.optional,
    // buttonContainerStyle: PropTypes.any.optional,
    // inputStyle
    // collapseValidationError :PropTypes.bool.optional,
    // highlightError    :PropTypes.bool.optional

}

const styles = StyleSheet.create({

    b:{
        borderWidth: 1,
        borderColor: "#2cc",
        borderStyle: 'dashed',
    },

    wrapper:{
        width: "100%",
        paddingHorizontal: 8,
        justifyContent:"flex-start",
        alignItems:"center",
    },

    wrapper1:{
        width: "100%",
        justifyContent:"flex-start",
        alignItems:"center",
    },


    errorText:{
        color: "#d22",
        fontSize: 12
    },

    containerView:{
        marginTop: 5,
        // marginVertical: 8,
    },

    socialButton:{
        height: 55,
        borderRadius:4,
        marginTop:0,
        marginBottom:0
    },
    buttonText:{

    },
    button:{

    }
});
