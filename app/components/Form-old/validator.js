import Form from "./Form"

export function isEmpty(str) {
    return (!str || 0 === str.length);
}

export function validateEmail(email) {
    //var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    let filter = /^([a-zA-Z0-9_\.\-])/;

    return filter.test(email);


}

export function validatePassword(password) {
    return password.length >= 6;


}

export function confirmPassword(c_password, password) {
    return c_password === password;


}

export function validatePhoneNumber(number){
    return true
}

export function validate(form) {
    let error = {};
    error.general = null;
    let success = true;

    let keys = Object.keys(form);
    let length = keys.length;

    keys.slice(0, length).map(field => {
        if (field !== "error" && field !== 'initialKey'){
            let { type, value } = form[field];
            if (isEmpty(value)){
                error[field] = type + ' is required';
                success = false;
            }else{
                error[field] = null;

                if (type === Form.FIELD_EMAIL && !validateEmail(value)) {
                    error[field] = 'Enter a valid email address';
                    success = false;
                }else if (type === Form.FIELD_PASSWORD && !validatePassword(value)) {
                    error[field] = 'Password must be at least 6 characters';
                    success = false;
                }else if (type === Form.FIELD_CONFIRM_PASSWORD && !confirmPassword(value, form["password"]['value'])) {
                    error[field] = 'Password does not match.';
                    success = false;
                }else if (type === Form.FIELD_PHONE_NUMBER && !validatePhoneNumber(value)) {
                    error[field] = 'Enter a valid phone number';
                    success = false;
                }
            }
        }
    });

    return {success, error};
}
