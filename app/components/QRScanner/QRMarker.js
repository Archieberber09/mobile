import React from "react";
import {View, StyleSheet} from "react-native";
import b from "../../styles/base";


const HEIGHT        = 200
const WIDTH         = 200
const BORDER_WIDTH  = 10
const BORDER_COLOR  = '#DDD'

export const QRMarker = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.captureSquare} >
                <View style={[styles.topMarker]}>
                    <View style={styles.topLeftEdge} />
                    <View style={styles.topRightEdge} />
                </View>
                <View style={[styles.bottomMarker]}>
                    <View style={styles.bottomLeftEdge} />
                    <View style={styles.bottomRightEdge} />
                </View>
            </View>
        </View>
    );
}

const styles =  StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    captureSquare: {
        height:  HEIGHT,
        width:  WIDTH,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    topMarker:{
        flexDirection: 'row',
        width:'100%',
        justifyContent: 'space-between',
    },

    bottomMarker:{
        flexDirection: 'row',
        width:'100%',
        justifyContent: 'space-between',
    },

    topLeftEdge: {
        height: 75,
        width: 75,
        borderColor: BORDER_COLOR,
        borderLeftWidth: BORDER_WIDTH,
        borderTopWidth: BORDER_WIDTH,
        // borderWidth:5,
        borderTopLeftRadius:35,
        overflow:'hidden'
    },
    topRightEdge: {
        height: 75,
        width: 75,
        borderColor: BORDER_COLOR,
        borderRightWidth: BORDER_WIDTH,
        borderTopWidth: BORDER_WIDTH,
        borderTopRightRadius: 35
    },
    bottomLeftEdge: {
        height: 75,
        width: 75,
        borderColor: BORDER_COLOR,
        borderLeftWidth: BORDER_WIDTH,
        borderBottomWidth: BORDER_WIDTH,
        borderBottomLeftRadius: 35
    },
    bottomRightEdge: {
        height: 75,
        width: 75,
        borderColor: BORDER_COLOR,
        borderRightWidth: BORDER_WIDTH,
        borderBottomWidth: BORDER_WIDTH,
        borderBottomRightRadius: 35
    },
    test: {
        position: 'absolute',
        top: 120,
        left: 190,
        height: 20,
        width: 0,
        borderColor: '#333',
        borderWidth: 5,
        borderRadius: 35
    },
})


