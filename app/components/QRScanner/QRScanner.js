import React from 'react';
import {Text, View, StyleSheet} from "react-native";
import PropTypes from 'prop-types'
import {Input, Button, Icon} from "react-native-elements";
import QRCodeScanner from "react-native-qrcode-scanner";
import {QRMarker} from "./QRMarker";

import b from '../../styles/base'


export default class QRScanner extends React.Component {

    constructor(props){
        super(props);

        this.onRead = this.onRead.bind(this);
    }


    onRead(data){
        this.props.onRead(data)
    }


    render(){
        let {showMarker, reactivate, onRead, topContent, bottomContent,
            style, cameraStyle, topContentStyle, bottomContentStyle
        } = this.props

        return (
            <QRCodeScanner
                fadeIn
                onRead          = {onRead}
                showMarker      = {showMarker}
                customMarker    = {<QRMarker/>}
                reactivate      = {reactivate}
                reactivateTimeout={10000}
                topContent      = {topContent}
                bottomContent   = {bottomContent}
                // cameraProps     = {{useCamera2Api:true}}

                containerStyle  ={style}
                cameraStyle     ={cameraStyle}
                topViewStyle    ={topContentStyle}
                bottomViewStyle ={bottomContentStyle}
            />
        )
    }
}