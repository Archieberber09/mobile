import React from 'react';
import {ImageBackground, StyleSheet, View} from "react-native";
import styles from "../styles/base";
import ActivityIndicator from "./ActivityIndicator";

/**
 * Refactoring considerations:
 * 1. pass ImageBackground source as props
 *
 * @returns {*}
 * @constructor
 */

export const SplashScreen = () => {
    return (
        <View style={styles.page}
              // source={require('../assets/images/siglo_bg_withIcon_draft.png')}
        >
            <View style={localStyles.containerSpace}/>
            <View style={localStyles.splashWrapper}>
                <ActivityIndicator/>
            </View>
            <View style={styles.container}/>
        </View>
    );
}


const localStyles =  StyleSheet.create({
    containerSpace:{
        ...styles.container,
        flex: 2
    },

    splashWrapper:{
        ...styles.container,
        width:"100%",
        height:"100%"
    }

});