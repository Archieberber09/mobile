export function extractData(data) {
    const retData = {};

    Object.keys(data).forEach(function (key) {
        if (key !== "error") {
            let {value} = data[key];
            retData[key] = value;
        }
    });

    return retData;
}