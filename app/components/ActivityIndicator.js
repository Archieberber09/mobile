// import LottieView from 'lottie-react-native';
import {StyleSheet, Text, View} from "react-native";
import React from "react";

/**
 * TREAT THIS AS A PRESENTATIONAL COMPONENT
 *
 * usage of componentWillMount() and other functions
 * suggests this as a container/smart component
 * However, only local animation processing is used
 *
 * Should you ever tweak this to control animation by adding outside props,
 * KINDLY refactor this to a container
 *
 * thank you! :)
 */

//use hook to remove class

class ActivityIndicator extends React.Component {

    state = {
        animation: null,
    };

    componentWillMount() {
         // this._playAnimation();
    }

    // _playAnimation = () => {
    //     if (!this.state.animation) {
    //         this._loadAnimationAsync();
    //     } else {
    //         this.animation.reset();
    //         this.animation.play();
    //     }
    // };

    // _loadAnimationAsync = () => {
    //     // refactor the require statement to be reusable
    //     // let result = this.props.animationAsset;
    //     let result = require('../assets/animated_images/demo_splash.json');
    //     this.setState({ animation: result }, this._playAnimation);
    // };
// <LottieView
// ref={animation => {
//     this.animation = animation;
// }}
// source={this.state.animation}
// style={localStyles.container}
// />

    render (){
        return (
            <View>
                <Text>Loading</Text>
            </View>
            );
    }

}

export default ActivityIndicator;



const localStyles =  StyleSheet.create({

    container:{
        height:'100%',
        width:'100%'
    }

});