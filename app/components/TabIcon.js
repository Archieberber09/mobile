import React from "react";
import {StyleSheet, Text, View} from "react-native";
import styles from "../styles/base";


export const TAB_ICON_HOME          = "Home";
export const TAB_ICON_SEARCH        = "Search";
export const TAB_ICON_SCAN          = "Scan";
export const TAB_ICON_STORE         = "Store";
export const TAB_ICON_NOTIFICATIONS = "Notification";


class TabIcon extends React.Component {
    render() {
        let src = "";
        switch(this.props.title){
            case TAB_ICON_HOME          : src = 'ios-home-outline'; break;
            case TAB_ICON_SEARCH        : src = 'ios-people-outline'; break;
            case TAB_ICON_SCAN          : src = 'ios-qr-scanner-outline'; break;
            case TAB_ICON_STORE         : src = 'ios-map-outline'; break;
            case TAB_ICON_NOTIFICATIONS : src = 'ios-notifications-outline'; break;
        }


        let title = this.props.title;

        return (
            <View style={styles.navIconContainer}>
                <Ionicons name={src} size={30} color="#000" />
                <Text style={styles.navIconLabel}>{title}</Text>
            </View>
        );
    }
}



const localStyles =  StyleSheet.create({
    navIconContainer:{
        ...styles.tab,
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        paddingVertical: 10,
    },

    navIconLabel:{
        // fontFamily: fontFamily.regular,
    },

});