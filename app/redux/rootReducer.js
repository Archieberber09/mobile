import { combineReducers } from 'redux';

import { reducer as authReducer } from "../modules/auth"
import { reducer as homeReducer } from "../modules/home"
import { reducer as scanReducer } from "../modules/scan"

// Combine all the reducers
const rootReducer = combineReducers({homeReducer, authReducer, scanReducer});

export default rootReducer;