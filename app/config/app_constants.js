// https://github.com/airbnb/javascript

const LOGIN = {
    fields : [
        {
            key: 'email',
            label: "Email Address",
            placeholder: "Email Address",
            autoFocus: false,
            secureTextEntry: false,
            value: "",
            type: "email"
        },
        {
            key: 'password',
            label: "Password",
            placeholder: "Password",
            autoFocus: false,
            secureTextEntry: true,
            value: "",
            type: "password"
        }
    ]
};

export {LOGIN}