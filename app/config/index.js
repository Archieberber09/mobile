import * as AppConstants from "./app_constants"
import * as OnesignalConfig from "./onesignal_config"

export const CONFIG = {AppConstants, OnesignalConfig}

export default CONFIG