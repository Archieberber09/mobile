import firebase from 'react-native-firebase';
//
/* Initialize Firebase
no need for this setup when you link natively

var config = {
    apiKey: "AIzaSyAP9-uAOp-hHOcxt6wnHtvYkyejHwgVAME",
    authDomain: "dlsu-univweek-2019.firebaseapp.com",
    databaseURL: "https://dlsu-univweek-2019.firebaseio.com",
    projectId: "dlsu-univweek-2019",
    storageBucket: "dlsu-univweek-2019.appspot.com",
    messagingSenderId: "641753502614"
};

firebase.initializeApp(config);
*/

// This code creates an instance of the Firebase SDK and configures it with your config.
// Now you can import it anywhere in your codebase and it’s always this singleton.
//     When you see Firebase from now on, assume that it’s imported from here.

// export const database = firebase.database();
// export const startTime = Firebase.database.ServerValue.TIMESTAMP;
// export const auth = firebase.auth();