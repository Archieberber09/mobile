import { StyleSheet } from 'react-native';
import { Dimensions, Platform } from 'react-native';
import {DEBUG} from "../config/APPCONFIG";


import { padding, color, fontSize, fontFamily, windowWidth, normalize } from "./theme"



export default base = StyleSheet.create({

    h : {//highlight
        borderWidth: DEBUG ? 1 : 0,
        borderColor: '#d700ff',
        borderStyle: 'dashed'
    },

    b : {//border
        borderWidth: DEBUG ? 1 : 0,
        borderColor: '#ffd700',
        borderStyle: 'dashed'
    },

    f:{//fill
        backgroundColor:DEBUG ? "rgba(30,30,30,0.5)" : null
    },

    facebookColor:{
        color:'#475993'
    },
    facebookBgColor:{
        backgroundColor:'#475993'

    },
    googleColor:{
        color:'#DC4E41'
    },
    googleBgColor:{
        backgroundColor:'#DC4E41'
    },

    background:{
      backgroundColor:"#FAFAFA"
    },

    page:{
        flex:1,
        alignItems:'center',
        justifyContent:'flex-start',
        flexDirection:'column',
        width:'100%',
        height:'100%',
        backgroundColor:'#fff'
    },

    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        // paddingTop: (Platform.OS) === 'ios' ? 0 : 24,
    },

    section:{
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
    },

    wrapper:{
        alignItems:'center',
        justifyContent:'center',
    },

    bottomContainer: {
        paddingVertical: padding,
        width: windowWidth,
        justifyContent: "flex-start",
        alignItems: "center",
    },

    bottom:{
        flexDirection: "row",
        justifyContent:"center",
        alignItems:"center",
        marginTop: padding * 2,
    },

    buttonContainer:{
        // backgroundColor: "pink",
        width: windowWidth - 60,
        justifyContent:"center",
        alignItems:"center",
    },

    titleContainer:{
        width: "100%",
        paddingHorizontal: 12,
        justifyContent: "flex-start",
        alignItems: "flex-start",
        alignContent: 'center'
    },

    tab:{
        width: windowWidth / 5,
    },

    title:{
        // fontSize: fontSize.larger,
        // lineHeight: fontSize.larger + 15,
        // fontFamily: fontFamily.heavy,
        // color:color.light_black,
        // marginBottom: 5,
        fontSize: 32,
        fontWeight: 'bold',
        color:'#333',
    },




    authBoxLayer2:{
        backgroundColor: '#EAEAEA',
        borderRadius: 15,

        shadowColor: "#111",
        shadowRadius: 15,
        shadowOpacity: 0.5,
        shadowOffset: {width: 5,height: 10 }
    },
    authBoxLayer1:{
        backgroundColor: '#fff',
        marginTop: 50,
        borderRadius: 15,
        justifyContent:"flex-start",
        alignItems:"center",
        width: 342,
        height: 472
    },
    authTitleContainer:{
        alignItems:'center',
        marginTop: 0,
        paddingTop: 50
    },
    authTitle:{
        fontWeight:'bold',
        fontSize:32
    },
    authButtonContainer:{
        borderRadius: 15,
        marginTop:20,
    },
    authButton:{
        backgroundColor:"#001019",//should be refactored to theme
        borderRadius: 15,
        height:29,
        width: 234,
    },
    authButtonText:{
        fontSize:8,
        fontWeight:'bold',
    },


    authSocialLoginContainer:{
        borderRadius: 15,
        overflow:'hidden',
        marginTop:15,
    },
    authSocialLoginButtonContainer:{
        borderWidth:0,
        borderRadius:0,
    },
    authSocialLoginButton:{
        height:31,
        width: 310,
        justifyContent:'flex-start',
        paddingLeft:0
    },
    authSocialLoginButtonText:{
        fontSize:8,
        fontWeight:'bold',
        color: 'white',
    },
    authSocialLoginIcon:{
        width:50,
        marginHorizontal:0,
    },


    authBottomText:{
        fontSize:14,
        color:'#333',
        paddingVertical:5
    },
    authBottomTextClickable:{
        fontSize:14,
        fontWeight:'bold',
        color:'#333',
        paddingVertical:5
    },


    authForm:{
        paddingHorizontal: 12,
        paddingTop:12
    },
    authFormField:{
        // borderRadius:15,
        // overflow:'hidden'
    },
    authFormInputContainer:{
        paddingHorizontal:0,
        marginBottom:10,
        marginVertical: 0,
    },
    authFormInput:{
        borderRadius:15,
        paddingLeft:5,
        height:31,
        borderBottomWidth: 0,
    },
    authFormInputContent:{
        fontSize:15,
        color:'#FAFAFA'
    },






    mainSection:{
        paddingLeft:15,
        paddingRight:15,
    },
    mainRectangle:{
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal: 15
    },
    mainSubContainerTitle: {
        width:'100%',
        alignItems:'flex-start'
    },
    mainSubContainerBottomTitle:{
        alignItems:'flex-end',
        width:'100%'
    },
    mainButtonContainer:{
        // borderRadius: 15
    },
    mainButton:{
        backgroundColor:"#333",//should be refactored to theme
        borderRadius: 15,
        height:30,
        width: 142
    },
    mainButtonText:{
        fontSize:12,
    },



    homeTitle:{

    },
    homeSubtitle:{
        fontSize: 14,
        fontWeight: 'bold',
        color:'#333',
    },
    homeTextLink:{
        fontSize: 14,
        color:'#333',
        fontWeight:'bold'
    },

    homeWalletTitle:{
        fontSize: 28,
        fontWeight: '100',
        color:'#333'
    },

    homeAnnouncementsContainer:{
        height: "33%",
        width: "100%",
        marginLeft:15,
    },

    homeTransactionsContainer:{
        height: "34%",
        width: "100%",
        marginTop: 30,
        marginLeft:15,
        paddingRight:15,
        // marginRight:15,
        marginBottom:20
    }
});





