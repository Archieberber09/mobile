export default class User {
  constructor(user) {
    this.id = user.id;
    this.name = user.name;
    this.email = user.email;
    this.number = user.number;
    this.emailVerified = user.email_verified_at;
    this.avatar = user.avatar;
    this.tokenType = user.token_type;
    this.accessToken = user.access_token;
    this.points = user.points;

    console.log("object created");
    this.print();
  }

  setAttributes(object) {
    console.log("setting attributes");
    for (let [key, value] of Object.entries(object)) {
      console.log(`${key}:`, value);
      switch (key) {
        case "email_verified_at":
          this.emailVerified = value;
          break;
        default:
          this[key] = value;
      }
    }
    this.print("DONE setting attributes\n");
    return this;
  }

  setTokenType = t => {
    this.tokenType = t;
    this.print("setTokenType(" + t + ")");
  };
  setAccessToken = a => {
    this.accessToken = a;
    this.print("setAccessToken(" + a + ")");
  };
  setPoints = b => {
    this.points = b;
    this.print("setPoints(" + b + ")");
  };

  getId = () => this.id;
  getName = () => this.name;
  getEmail = () => this.email;
  isEmailVerified = () => this.emailVerified;
  getAvatar = () => this.avatar;
  getTokenType = () => this.tokenType;
  getAccessToken = () => this.accessToken;
  getPoints = () => this.points;

  print = message => console.log("PRINT()\n", message, this);
}
