export default class Transaction {

    constructor(transaction){
        console.log(transaction)

        this.id         = transaction.id;
        this.weight     = transaction.weight;
        this.points     = transaction.points;
        this.dateCreated= transaction.dateCreated;
        this.dateUpdated= transaction.dateUpdated;

        //console.log('object created')
        //this.print();
    }

    setAttributes(object){
        console.log('setting attributes')
        for (let [key, value] of Object.entries(object)) {
            console.log(`${key}: ${value}`);
        }
        this.print('DONE setting attributes\n')
    }

    getId       =()=>   this.id;
    getWeight   =()=>   this.weight;
    getPoints   =()=>   this.points;
    getDateCreated  =()=>   this.dateCreated;
    getDateUpdated  =()=>   this.dateUpdated;

    print       =(message)=> console.log(message, this);
}