/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with how things WORK.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here

class LoginPage extends React.Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){

    }

    render() {
        return (null);
    }
}

/* UNCOMMENT and EDIT

// use this so you dont hae to debug only to
// know you passed a text instead of a function
LoginPage.propTypes = {
    template: PropTypes.func,

};

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
    template: state.template,
});

const mapDispatchToProps = (dispatch) => ({
    setTitle: (title) => dispatch(setTitle(title)),
    setText: (text) => functionFromActionsFile,
});


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
*/