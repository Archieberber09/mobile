import React from 'react';
import {connect} from 'react-redux';
// import PropTypes from 'prop-types';

import {Routes} from "../scenes_design/Routes";
import {bindActionCreators} from "redux";
import {Text, View} from "react-native";



class RoutesPage extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {

    }

    render() {
        return (
            <Routes
                isLoggedIn = {this.props.isLoggedIn}
            />
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    isLoggedIn:      state.authReducer.isLoggedIn
});

function mapDispatchToProps(dispatch) {
    return {
        // getCountDownTime    : bindActionCreators(calculateEventCountdownTime, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(RoutesPage);