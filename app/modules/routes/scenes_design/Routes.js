import React from 'react';
import {Text, View, SafeAreaView} from "react-native";
import { Scene, Router, Stack,} from 'react-native-router-flux';
import {Icon} from "react-native-elements";

import b from '../../../styles/base'
import {LoginPage, RegisterPage,VerifyEmailPage,ForgotPasswordPage,CompleteProfilePage} from "../../auth";
import HomePage from "../../home/scenes/HomePage";
import ScanPage from "../../scan/scenes/ScanPage";
import WalletPage from "../../wallet/scenes/WalletPage";
import SettingsPage from "../../settings/scenes/SettingsPage";
import {DEBUG_SHOW__ALL_SCREENS} from "../../../config/APPCONFIG";
import ConfirmedScanPage from "../../scan/scenes/ConfirmedScanPage";
import WelcomePage from "../../auth/scenes/WelcomePage";
import RejectedScanPage from "../../scan/scenes/RejectedScanPage";


const TABS = {
    Home:   {src: 'ios-home',       type:'ionicon'},
    Scan:   {src:'ios-qr-scanner',  type:'ionicon'},
    Wallet: {src: 'ios-wallet',     type:'ionicon'},
    Settings:{src: 'ios-settings',  type:'ionicon'},
};


//now refactored to functional component
const TabIcon = (props) => {
    let {src, type} = (TABS[props.title])?TABS[props.title]:{src:"",type:""};
    let color = '#FAFAFA'// (props.focused)?'white':'#333'

    return (
        <View style={[b.b, b.container,{paddingVertical:10}]}>
            <Icon name={src} type={type} containerStyle={[b.b]} size={23} color={color}/>
            <Text style={[{fontSize:12,fontWeight:'bold',color:color},b.b]}>
                {props.title.toLowerCase()}
                </Text>
        </View>
    );
}


export const Routes = (props) => {

    return (
        <Router>
            {
                (!DEBUG_SHOW__ALL_SCREENS)?
                    (
                    <Scene key="root"
                           hideNavBar
                           navigationBarStyle={{backgroundColor: "rgba(0,0,0,0.5)"}}
                           backButtonTintColor={"#000"}>
                        <Stack key="Auth" initial={!props.isLoggedIn}>
                            {/*<Scene key='LoginRegister' component={LoginRegisterPage} initial hideNavBar/>*/}
                            {/*<Scene key="Welcome" title= 'Welcome'component={WelcomePage} hideNavBar initial/>*/}
                            <Scene key="Login" component={LoginPage} hideNavBar initial/>
                            <Scene key="Register" component={RegisterPage}  hideNavBar/>
                            <Scene key="VerifyEmail" component={VerifyEmailPage} hideNavBar/>
                            <Scene key="ForgotPassword" component={ForgotPasswordPage} hideNavBar/>
                            <Scene key="CompleteProfile" component={CompleteProfilePage} hideNavBar />
                        </Stack>

                        <Stack key="Main" initial={props.isLoggedIn} >
                            <Scene key="MainTabs"
                                   tabs
                                   tabBarStyle={{backgroundColor: '#102C57', height: 60}}
                                   tabBarPosition='bottom'
                                   activeBackgroundColor = '#0E264B'
                                   showLabel={false}
                                   hideNavBar
                            >
                                <Scene key="Home" title= 'Home'component={HomePage} icon={TabIcon} initial  hideNavBar/>
                                <Scene key="Scan" title= 'Scan'component={ScanPage} icon={TabIcon} hideNavBar/>
                                <Scene key="Wallet" title='Wallet'component={WalletPage} icon={TabIcon} hideNavBar />
                                <Scene key='Settings' title='Settings'component={SettingsPage} icon={TabIcon} hideNavBar>
                                </Scene>
                            </Scene>
                            <Scene key="ConfirmedScan" title= 'ConfirmedScan'component={ConfirmedScanPage} hideNavBar/>
                            <Scene key="RejectedScan" title= 'RejectedScan'component={RejectedScanPage} hideNavBar/>
                        </Stack>
                    </Scene>
                    )
                    :
                    (
                    <Scene key="DEBUG_TABS"
                           tabs
                           tabBarPosition='top'
                           showLabel={false}
                           tabBarComponent={() => null}
                           swipeEnabled
                           hideNavBar
                        >
                            {/*<Scene key="Login" component={LoginPage} icon={TabIcon} hideNavBar/>*/}
                            {/*<Scene key="Register" component={RegisterPage} icon={TabIcon} hideNavBar/>*/}
                            {/*<Scene key="VerifyEmail" title='verify' component={VerifyEmailPage} icon={TabIcon} hideNavBar/>*/}
                            {/*<Scene key="ForgotPassword" title='forgot' component={ForgotPasswordPage} icon={TabIcon} hideNavBar />*/}
                            {/*<Scene key="CompleteProfile" title='complete' component={CompleteProfilePage} icon={TabIcon} hideNavBar />*/}
                            {/*<Scene key="Main" component={HomePage}  hideNavBar/>*/}
                            <Scene initial key="Scan" title= 'Scan'component={ScanPage} icon={TabIcon} hideNavBar/>
                    </Scene>
                    )
            }
        </Router>
    )
}