import React from 'react';
import {View, Text} from "react-native";

import b from "../../../styles/base";
import {Form} from "../../../components";


export const ForgotPassword = (props) => {
    return (
        <View
            // source={assets.BACKGROUND_IMAGE}
            style={[b.container,b.background]}>
            <View style={[b.container]}>
                <View elevation={5}
                      style={[b.b,b.authBoxLayer2]}
                >
                    <View style={[b.b,b.authBoxLayer1,{height:277, justifyContent:'flex-start'}]}>
                        <View style={[b.b,b.authTitleContainer]}>
                            <Text style={[b.b,b.title,b.authTitle,{fontSize:27}]}>FORGOT PASSWORD</Text>
                        </View>
                        <Form
                            fields={props.formFields}
                            buttonTitle="SUBMIT"
                            onSubmitForm={props.onSubmitForm}
                            error={props.error}
                            showLabel={props.showLabel}
                            highlightError={props.highlightError}
                            collapseValidationError={props.collapseValidationError}

                            style={[b.b, b.authForm, {marginVertical:15}]}
                            fieldStyle={[b.b, b.authFormField]}
                            inputContainerStyle={[b.b,b.authFormInputContainer]}
                            inputStyle={[b.b,b.authFormInput]}
                            inputContentStyle={[b.b,b.authFormInputContent]}
                            buttonContainerStyle={[b.authButtonContainer]}
                            buttonStyle={[b.authButton]}
                            buttonTextStyle={[b.authButtonText]}
                        />
                            <Text style={[b.authBottomTextClickable]} onPress={props.onBackClick}>
                                back
                            </Text>
                    </View>
                </View>
            </View>
        </View>
    );
}


