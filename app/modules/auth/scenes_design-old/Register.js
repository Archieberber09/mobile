import React from 'react';
import {View, Text} from "react-native";

import {Form} from "../../../components";
import b from "../../../styles/base";


export const Register = (props) => {
    return (
        <View
            // source={assets.BACKGROUND_IMAGE}
            style={[b.container, b.background]}>
            <View style={[b.container]}>
                <View elevation={5}
                      style={[b.b,b.authBoxLayer2]}
                >
                        <View style={[b.h,b.authBoxLayer1,]}>
                            <View style={[b.h,b.titleContainer,b.authTitleContainer]}>
                                <Text style={[b.b,b.title,b.authTitle]}>REGISTER</Text>

                            </View>
                            <Form uniqueKey           = {props.formKey}
                                    fields={props.formFields}
                                  buttonTitle="SUBMIT"
                                  onSubmitForm={props.onSubmitForm}
                                  error={props.error}
                                  showLabel={false}
                                  collapseValidationError={props.collapseValidationError}
                                  highlightError={props.highlightError}

                                  style={[b.b, b.authForm]}
                                  fieldStyle={[b.b, b.authFormField]}
                                  inputContainerStyle={[b.b,b.authFormInputContainer]}
                                  inputStyle={[b.authFormInput]}
                                  inputContentStyle={[b.authFormInputContent]}
                                  buttonContainerStyle={[{},b.authButtonContainer,b.h]}
                                  buttonStyle={[{},b.authButton]}
                                  buttonTextStyle={[b.authButtonText]}
                            />
                            <View style={[b.container]}>
                                <View style={[b.authSocialLoginContainer]}>
                                    <Text style={[b.authBottomTextClickable]} onPress={props.onBackClick}>
                                        back
                                    </Text>
                                </View>
                            </View>
                        </View>
                </View>
            </View>
        </View>
    );
}

/*
<View
                // source={assets.BACKGROUND_IMAGE}
                style={[b.container, b.background]}>
                <View style={[b.container]}>
                    <View elevation={5}
                          style={[b.b,b.authBoxLayer2]}
                    >


 */
