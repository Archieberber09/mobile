import React from 'react';
import {View, Text} from "react-native";
import {Button} from 'react-native-elements'

import {Form} from "../../../components";
import b from "../../../styles/base";



export const Login = (props) => {
    return (
        <View
            // source={assets.BACKGROUND_IMAGE}
            style={[b.container,b.background]}>
            <View style={[b.container]}>
                <View elevation={5}
                      style={[b.b,b.authBoxLayer2]}
                >
                              <View style={[b.b,b.authBoxLayer1,{height:400}]}>
                                  <View style={[b.b,b.titleContainer,b.authTitleContainer]}>
                                      <Text style={[b.b,b.title,b.authTitle]}>LOG-IN</Text>
                                  </View>
                                  <Form uniqueKey           = {props.formKey}
                                        fields              = {props.formFields}
                                        buttonTitle         = "SUBMIT"
                                        onSubmitForm    = {props.onSubmitForm}
                                        error           = {props.error}
                                        showLabel       = {props.showLabel}
                                        highlightError  = {props.highlightError}
                                        collapseValidationError={props.collapseValidationError}

                                        style={[b.b, b.authForm]}
                                        fieldStyle={[b.b, b.authFormField]}
                                        inputContainerStyle={[b.b,b.authFormInputContainer]}
                                        inputStyle={[b.b,b.authFormInput]}
                                        inputContentStyle={[b.b,b.authFormInputContent]}
                                        buttonContainerStyle={[b.authButtonContainer]}
                                        buttonStyle={[b.authButton]}
                                        buttonTextStyle={[b.authButtonText]}
                                  />
                                  <View style={[b.container]}>
                                      <View style={[b.container]}>
                                      {/*<View style={[b.authSocialLoginContainer]}>*/}
                                          {/*<Button*/}
                                              {/*iconLeft*/}
                                              {/*icon={{*/}
                                                  {/*name: "facebook",*/}
                                                  {/*size: 15,*/}
                                                  {/*color: "white",*/}
                                                  {/*type: 'font-awesome'*/}
                                              {/*}}*/}
                                              {/*title='Log-in with Facebook'*/}
                                              {/*onPress={props.onFBLoginClick}*/}

                                              {/*containerStyle={[b.facebookBgColor,b.authSocialLoginButtonContainer]}*/}
                                              {/*iconContainerStyle={[b.authSocialLoginIcon]}*/}
                                              {/*buttonStyle={[b.facebookBgColor,b.authSocialLoginButton]}*/}
                                              {/*titleStyle={[b.authSocialLoginButtonText]}*/}
                                          {/*/>*/}
                                          {/*<Button*/}
                                              {/*iconLeft*/}
                                              {/*icon={{*/}
                                                  {/*name: "google",*/}
                                                  {/*size: 15,*/}
                                                  {/*color: "white",*/}
                                                  {/*type: 'font-awesome'*/}
                                              {/*}}*/}
                                              {/*title='Log-in with Google'*/}
                                              {/*onPress={props.onGoogleLoginClick}*/}

                                              {/*containerStyle={[b.b,b.googleBgColor,b.authSocialLoginButtonContainer]}*/}
                                              {/*iconContainerStyle={[b.b,b.authSocialLoginIcon]}*/}
                                              {/*buttonStyle={[b.b,b.googleBgColor,b.authSocialLoginButton]}*/}
                                              {/*titleStyle={[b.b,b.authSocialLoginButtonText]}*/}
                                          {/*/>*/}
                                      {/*</View>*/}
                                      </View>
                                      <View style={[b.container]}>
                                          <Text style={[b.authBottomTextClickable]} onPress={props.onForgotPasswordClicked}>
                                              forgot password?
                                          </Text>
                                      </View>
                                  </View>

                                  <View style={[b.h,b.wrapper,{marginBottom:10}]}>
                                      <View>
                                      <Button
                                          containerStyle={[{},b.authButtonContainer]}
                                          buttonStyle={[{},b.authButton]}
                                          titleStyle={[b.authButtonText]}
                                          title='REGISTER'
                                          onPress={props.onRegisterClicked}
                                      />
                                      </View>
                                  </View>

                              </View>
                </View>
            </View>
        </View>
    );
}


/*
const form = createForm({config})
const logo = logo
const components = {logo, form}

return (loop through components)



 */