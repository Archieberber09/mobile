import * as actions from './actions';
import reducer from './reducer';


import LoginPage from "./scenes/LoginPage";
import RegisterPage from "./scenes/RegisterPage";
import VerifyEmailPage from "./scenes/VerifyEmailPage";
import ForgotPasswordPage from "./scenes/ForgotPasswordPage";
import CompleteProfilePage from "./scenes/CompleteProfilePage";

import * as theme from '../../styles/theme';

export { actions, reducer, theme,
    LoginPage, RegisterPage, VerifyEmailPage, ForgotPasswordPage, CompleteProfilePage
};