import React from 'react';
import {View, Text, ImageBackground} from "react-native";
import {Button} from 'react-native-elements'

import {Form} from "../../../components";
import b from "../../../styles/base";
import assets from "../../../assets";



export const Welcome = (props) => {
    return (
        <ImageBackground
            source={assets.BACKGROUND_IMAGE}
            style={[b.b,b.container,b.background]}>
            <View style={[b.b]}>
                <Text>
                    <Text style={[{fontSize:70, color:'#E4E4E4'}]}>
                        Clean
                    </Text>
                    <Text>{"    "}</Text>
                    <Text style={[{fontSize:15, color:'#E4E4E4'}]}>
                         our
                    </Text>
                </Text>
                <Text style={[{fontSize:70, color:'#E4E4E4'}]}>
                    Oceans
                </Text>
            </View>
            <Button
                containerStyle={[{},b.authButtonContainer]}
                buttonStyle={[{},b.authButton]}
                titleStyle={[b.authButtonText]}
                title='Log-in'
                onPress={props.onLoginClick}/>
            <Button
                containerStyle={[{},b.authButtonContainer]}
                buttonStyle={[{},b.authButton]}
                titleStyle={[b.authButtonText]}
                title='Register'
                onPress={props.onRegisterClick}/>

        </ImageBackground>
    );
}
