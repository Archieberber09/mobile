import React from 'react';
import {View, Text, ImageBackground} from "react-native";

import b from "../../../styles/base";
import {Form} from "../../../components";
import assets from "../../../assets";


export const ForgotPassword = (props) => {
    return (
        <ImageBackground
            source={assets.BACKGROUND_IMAGE}
            style={[b.container,b.background]}>
            <View style={[b.container]}>
                <View style={[b.b,b.authTitleContainer]}>
                            <Text style={[b.b,b.title,b.authTitle,{fontSize:27}]}>Forgot Password</Text>
                        </View>
                        <Form
                            fields={props.formFields}
                            buttonTitle="SUBMIT"
                            onSubmitForm={props.onSubmitForm}
                            error={props.error}
                            showLabel={props.showLabel}
                            highlightError={props.highlightError}
                            collapseValidationError={props.collapseValidationError}
                            placeholderTextColor = '#FAFAFA'

                            style={[b.b, b.authForm, {marginVertical:15}]}
                            fieldStyle={[b.b, b.authFormField]}
                            inputContainerStyle={[b.b,b.authFormInputContainer]}
                            inputStyle={[b.b,b.authFormInput]}
                            inputContentStyle={[b.b,b.authFormInputContent]}
                            buttonContainerStyle={[b.authFormButtonContainer,]}
                            buttonStyle={[b.authFormButton,{width:155}]}
                            buttonTextStyle={[b.authFormButtonText]}
                        />
                            <Text style={[b.authBottomTextClickable]} onPress={props.onBackClick}>
                                back
                            </Text>
                    </View>
        </ImageBackground>
    );
}


