import React from 'react';
import {View, Text} from "react-native";

import {Form} from "../../../components";
import b from "../../../styles/base";



export const CompleteProfile = (props) => {

    /*
    const form = createForm({config})
    const logo = logo
    const components = {logo, form}

    return (loop through components)
     */

    return (

        <View
            // source={assets.BACKGROUND_IMAGE}
            style={[b.container,b.background]}>
            <View style={[b.container]}>
                <View elevation={5}
                      style={[b.b,b.authBoxLayer2]}
                >
                    <View style={[b.b,b.authBoxLayer1,{height:400}]}>
                        <View style={[b.b,b.titleContainer,b.authTitleContainer]}>
                            <Text style={[b.b,b.title,b.authTitle,{fontSize:27}]}>COMPLETE PROFILE</Text>
                        </View>
                        <Form uniqueKey           = {props.formKey}
                              fields              = {props.formFields}
                              buttonTitle         = "SUBMIT"
                              onSubmitForm    = {props.onSubmitForm}
                              error           = {props.error}
                              showLabel       = {props.showLabel}
                              highlightError  = {props.highlightError}
                              collapseValidationError={props.collapseValidationError}

                              style={[b.b, b.authForm]}
                              fieldStyle={[b.b, b.authFormField]}
                              inputContainerStyle={[b.b,b.authFormInputContainer]}
                              inputStyle={[b.b,b.authFormInput]}
                              inputContentStyle={[b.b,b.authFormInputContent]}
                              buttonContainerStyle={[b.authFormButtonContainer]}
                              buttonStyle={[b.authFormButton]}
                              buttonTextStyle={[b.authFormButtonText]}
                        />
                        <View style={[b.container]}>
                            <View style={[b.authSocialLoginContainer]}>
                                <Text style={[b.authBottomTextClickable]} onPress={props.onBackClick}>
                                    back
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}


