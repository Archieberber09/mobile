import React from 'react';
import {View, Text, ImageBackground} from "react-native";

import {Form} from "../../../components";
import b from "../../../styles/base";
import assets from "../../../assets";

export const Register = (props) => {
    return (
        <ImageBackground
            source={assets.BACKGROUND_IMAGE}
            style={[b.container, b.background]}
            resizeMode='stretch'
        >
            <View style={[b.container]}>
                <View style={[b.h,b.titleContainer,b.authTitleContainer]}>
                    <Text style={[b.b,b.title,b.authTitle]}>Register</Text>
                            </View>
                            <Form uniqueKey           = {props.formKey}
                                    fields={props.formFields}
                                  buttonTitle="SUBMIT"
                                  onSubmitForm={props.onSubmitForm}
                                  error={props.error}
                                  showLabel={false}
                                  collapseValidationError={props.collapseValidationError}
                                  highlightError={props.highlightError}
                                  placeholderTextColor = '#FAFAFA'

                                  style={[b.b, b.authForm]}
                                  fieldStyle={[b.b, b.authFormField]}
                                  inputContainerStyle={[b.b,b.authFormInputContainer]}
                                  inputStyle={[b.authFormInput]}
                                  inputContentStyle={[b.authFormInputContent]}
                                  buttonContainerStyle={[{},b.authFormButtonContainer,b.h]}
                                  buttonStyle={[{},b.authFormButton]}
                                  buttonTextStyle={[b.authFormButtonText]}
                            />
                            <View style={[b.wrapper]}>
                                <View style={[b.authSocialLoginContainer]}>
                                    <Text style={[b.authBottomTextClickable]} onPress={props.onBackClick}>
                                        back
                                    </Text>
                                </View>
                            </View>
                        </View>
        </ImageBackground>
    );
}

/*
<View
                // source={assets.BACKGROUND_IMAGE}
                style={[b.container, b.background]}>
                <View style={[b.container]}>
                    <View elevation={5}
                          style={[b.b,b.authBoxLayer2]}
                    >


 */
