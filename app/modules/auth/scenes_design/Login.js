import React from 'react';
import {View, Text, ImageBackground, Image, Dimensions} from "react-native";
import {Button} from 'react-native-elements'

import {Form} from "../../../components";
import b from "../../../styles/base";
import assets from "../../../assets";



export const Login = (props) => {
    return (
        <ImageBackground
            source={assets.BACKGROUND_IMAGE}
            style={[b.b,b.container,b.background]}
            resizeMode='stretch'
        >
            <View style={[b.b,{alignItems:'center', width:'100%'}]}>
                <Image
                    source={assets.LOGO}
                    style={[b.b,{width:120, height:120,resizeMode:'contain'}]}
                />
                <Text style={[{fontSize:15, color:'#E4E4E4'}]}>
                    CLEAN OUR OCEAN PROJECT
                </Text>
            </View>
            <View style={[b.h,b.titleContainer,b.authTitleContainer]}>
                <Text style={[b.b,b.title,b.authTitle, {marginTop: 30}]}>
                    Login</Text>
            </View>
            <Form uniqueKey           = {props.formKey}
                  fields              = {props.formFields}
                  buttonTitle         = "SUBMIT"
                  onSubmitForm    = {props.onSubmitForm}
                  error           = {props.error}
                  showLabel       = {props.showLabel}
                  highlightError  = {props.highlightError}
                  collapseValidationError={props.collapseValidationError}
                  placeholderTextColor = '#FAFAFA'

                  style={[b.b, b.authForm]}
                  fieldStyle={[b.b, b.authFormField]}
                  inputContainerStyle={[b.b,b.authFormInputContainer]}
                  inputStyle={[b.b,b.authFormInput]}
                  inputContentStyle={[b.b,b.authFormInputContent]}
                  buttonContainerStyle={[b.authFormButtonContainer]}
                  buttonStyle={[b.authFormButton]}
                  buttonTextStyle={[b.authFormButtonText]}
            />
            <Text style={[b.authBottomTextClickable,
                {marginTop:20, marginBottom:35}]}
                  onPress={props.onForgotPasswordClicked}>
                {'forgot password?'.toUpperCase()}
            </Text>
            <View style={[b.h,b.wrapper,{marginBottom:10}]}>
                <View>
                    <Button
                        containerStyle={[{},b.authFormButtonContainer]}
                        buttonStyle={[{},b.authFormButton]}
                        titleStyle={[b.authFormButtonText]}
                        title='REGISTER'
                        onPress={props.onRegisterClicked}
                    />
                </View>
            </View>
            {/*<View style={[b.wrapper]}>*/}
                {/*<View style={[b.authSocialLoginContainer]}>*/}
                    {/*<Text style={[b.authBottomTextClickable]} onPress={props.onBackClick}>*/}
                        {/*back*/}
                    {/*</Text>*/}
                {/*</View>*/}
            {/*</View>*/}
        </ImageBackground>
    );
}


/*
const form = createForm({config})
const logo = logo
const components = {logo, form}

return (loop through components)



 */