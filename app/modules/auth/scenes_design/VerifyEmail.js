import React from 'react';
import {View, StyleSheet, Platform, Text, ImageBackground, TouchableOpacity} from "react-native";
import b from "../../../styles/base";
import {Button} from "react-native-elements";
import assets from "../../../assets";

// import styles from '../../../styles/b'
// import Form from "../../../components/Form";
// import {Icon} from "react-native-elements";
// import * as theme from "../../../styles/theme"
// const resizeMode = 'contain';
// const  { color, padding, windowWidth, normalize, fontSize, fontFamily } = theme;
// import Form from "../../../components/Form/Form";

export const VerifyEmail = (props) => {
    return (
        <ImageBackground
            source={assets.BACKGROUND_IMAGE}
            style={[b.container, b.background]}
            resizeMode='stretch'
        >
            <View style={[b.container]}>
                <View style={[b.b,b.authTitleContainer]}>
                            {/*<Text style={[b.b,b.title,b.authTitle,{fontSize:18, paddingHorizontal:20}]}>*/}
                                {/*Please check your email to proceed to the next step of the registration process.*/}
                            {/*</Text>*/}
                    <Text style={[b.b,b.title,b.authTitle,{fontSize:18, paddingHorizontal:20}]}>
                        Account created. Please proceed to log in.
                    </Text>
                        </View>
                        <View style={[b.h,b.wrapper,{marginBottom:10}]}>
                            <View>
                                <Button
                                    containerStyle={[{},b.authButtonContainer]}
                                    buttonStyle={[{},b.authButton]}
                                    titleStyle={[b.authButtonText]}
                                    title='Continue'
                                    onPress={props.onProceedClick}
                                />
                            </View>
                        </View>
                        {/*<Text>*/}
                            {/*<Text style={[b.authBottomText]}>*/}
                                {/*No email received?*/}
                            {/*</Text>*/}
                            {/*<Text style={[b.authBottomTextClickable]} onPress={props.onResendVerifyClick}>*/}
                                {/*Resend*/}
                            {/*</Text>*/}
                        {/*</Text>*/}
                    </View>
        </ImageBackground>
    );
}

/*
    <ImageBackground
            source={ require('../../../../assets/images/theme-bg.png')}
            style={styles.container}
        >
            <View style={styles.topContainer}>
                <TouchableOpacity hitSlop={{top: 20, bottom: 20, left: 40, right: 40}}
                                  onPress={ () => {
                                      goBack();
                                  }}>
                    <Ionicons name="ios-arrow-back" size={24} color="#fff" />
                </TouchableOpacity>
            </View>

            <View style={styles.logoContainer}>
                <Image
                    source={ require('../../../../assets/images/ahro-logo-white.png') }
                    style={styles.logo} />
            </View>
        </ImageBackground>

        <ImageBackground
            source = {require('../../../../assets/images/theme-bg.png')}
            style = {{width: "100%", height: "100%", flex: 1, justifyContent: "center", alignItems: "center", }}>
            <View>
                <View>
                    <View>
                        <Text>Please check your email to proceed to the next step of the registration process.</Text>
                        <TouchableOpacity onPress={this.props.onSignOut}>
                            <Text>Continue</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    Or verify here if no email received or is expired
                </View>
            </View>
        </ImageBackground>
     */

/*
const form = createForm({config})
const logo = logo
const components = {logo, form}

return (loop through components)
 */