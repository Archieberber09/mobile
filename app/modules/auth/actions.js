import AsyncStorage from '@react-native-community/async-storage';
import * as adapter from "../../adapter";
import {GET_TRANSACTIONS_SUCCESS, GET_WALLET_SUCCESS} from "../home/actions";

export const LOGGED_IN = 'auth/LOGGED_IN';
export const LOGGED_OUT = 'auth/LOGGED_OUT';
export const LOGIN_ERROR =   'auth/LOGIN_ERROR';
export const LOGOUT_ERROR =   'auth/LOGOUT_ERROR';
export const REGISTER_REQUEST =   'auth/REGISTER_REQUEST';
export const REGISTER_ERROR =   'auth/REGISTER_ERROR';
export const VERIFY_EMAIL_ERROR = 'auth/VERIFY_EMAIL_ERROR'
export const FORGOT_PASSWORD_ERROR = 'auth/FORGOT_PASSWORD_ERROR';
export const COMPLETE_PROFILE_ERROR = 'auth/COMPLETE_PROFILE_ERROR';
export const REFRESH_USER   = 'auth/REFRESH_USER'

// export const REFRESH_FORM



export function login(data, onLoginSuccess){
    return (dispatch) => {
        console.log("actions.login")

        adapter.Auth.mlogin(data)
            .then((user)=>{
                dispatch({type: LOGGED_IN, data: user})                
                AsyncStorage.setItem('user', JSON.stringify(user));

                user.print('USER AUTH LOGIN')

                return adapter.Database.mgetUser(user);
            })
            .then((user)=>{
                console.log('DATABASE MGETUSER')
                console.log(user);
                dispatch({type: GET_WALLET_SUCCESS, data:user})

                return adapter.Database.mgetWallet(user)
            })
            .then((user)=>{
                dispatch({type: GET_WALLET_SUCCESS, data:user})

                return adapter.Database.mgetTransactions(user)
            })
            .then((transactions)=>{
                dispatch({type: GET_TRANSACTIONS_SUCCESS, data: transactions})

                console.log("onLoginSuccess()")
                onLoginSuccess();
            })
            .catch((error)=>{
            console.log("dispatched error is: "+error)
            dispatch({type: LOGIN_ERROR, data: error, date: (new Date()).getTime()});
        });
    };
}

export function logout(data, onLogoutSuccess){
    return (dispatch) => {
        console.log("actions.logout")
        AsyncStorage.removeItem('user');
        dispatch({type: LOGGED_OUT})
        onLogoutSuccess()
        // adapter.Auth.mlogout(data).then(()=>{
        //     AsyncStorage.removeItem('user');
        //     dispatch({type: LOGGED_OUT})
        //     onLogoutSuccess()
        // }).catch((error)=>{
        //     console.log("dispatched error is: "+error)
        //     dispatch({type: LOGOUT_ERROR, data: error});
        // });
    }
}

export function register(data, onRegisterSuccess){
    return (dispatch) => {
        console.log("actions.register");

        dispatch({type: REGISTER_REQUEST});

        adapter.Auth.mregister(data)
            .then((user)=>{
                console.log("mregister success")
                return adapter.Database.mcreateUser(user, data)})
            .then(()=>{
                console.log("mcreateuser success")
                //return adapter.Auth.msendEmailVerification()})
            //.then(()=>{
                console.log("msendemailverification success");
                // dispatch({type: REGISTER_SUCCESS, data: user})
                onRegisterSuccess();
            })
            .catch((error)=>{
                console.log("dispatched error is: "+error)
                dispatch({type: REGISTER_ERROR, data: error, date: (new Date()).getTime(), disableForm: false});
        })
    }
}

export function sendVerify(data, onSendVerifySuccess){
    return (dispatch) => {
        adapter.Auth.msendEmailVerification()
            .then(()=>{
                console.log("msendemailverification success");
                // dispatch({type: LOGGED_IN, data: user})
                onSendVerifySuccess();})
            .catch((error)=>{
                console.log("dispatched error is: "+error)
                dispatch({type: VERIFY_EMAIL_ERROR, data: error, date: (new Date()).getTime()});
            })
    }
}

export function resetPassword(data, onResetPasswordSuccess){
    return (dispatch) => {
        console.log('actions.resetPassword')

        adapter.Auth.mresetPassword(data)
            .then(()=>{
                console.log("mresetpassword success");
                // dispatch({type: LOGGED_IN, data: user})
                onResetPasswordSuccess();
            })
            .catch((error)=>{
                console.log("dispatched error is: "+error)
                dispatch({type: FORGOT_PASSWORD_ERROR, data: error, date: (new Date()).getTime()});
            })
    }
}

export function completeProfile(data, onCompleteProfileSuccess){
    return (dispatch) => {

    }
}




















// export function register(data, successCB) {
//     return (dispatch) => {
//         api.register(data, function (success, data, error) {
//             if (success) successCB(data);
//             else if (error) {console.log(error);dispatch({type: REGISTER_ERROR, data: error.toString()});}
//         });
//     };
// }
//
// export function createUser(user, successCB, errorCB) {
//     return (dispatch) => {
//         // dispatch({type: t2.RESET_POINTS});
//         api.createUser(user, function (success, data, error) {
//                 if (success) {
//                     dispatch({type: LOGGED_IN, data: user});
//                     successCB();
//                 }else if (error) errorCB(error)
//             },
//             (newKey, valtype) => {
//                 dispatch({type: valtype.type, key: newKey});
//             });
//     };
// }
//
// export function login(data, successCB, verifyCB) {
//     return (dispatch) => {
//         // dispatch({type: RESET_POINTS});
//
//         const callback = function (isSuccess, APIdata, APIerror, isVerified) {
//             console.log("callback"+isSuccess+" "+APIdata+" "+isVerified)
//             if (isSuccess) {
//                 if(isVerified){
//                     if (APIdata.exists) {
//                         dispatch({type: LOGGED_IN, data: APIdata.user});
//
//                         // Save token and data to Asyncstorage
//                         AsyncStorage.multiSet([
//                             ['user', JSON.stringify(user)]
//                         ]);
//                     }
//                     console.log(APIdata)
//                     successCB(APIdata);
//                 }
//                 else{
//                     verifyCB(APIdata.user);
//                     //callback() returns Logic
//                     //verifyCB(utils.parseuser())
//                 }
//             }else if (APIerror) dispatch({type: LOGIN_ERROR, data: APIerror});
//             //errorCB(firebaseutils.parseAndReorganizeErrors(APIerror))
//             // returns errorFile instance. errorFile is global config of the project
//         };
//
//         console.log("api.login calling firebase function")
//
//         api.login(data, callback);
//     };
// }
//
//
// export function loginv2(data, {onLoginSuccess, onLoginError}) {
//     return (dispatch) => {
//         /*
//         1 login
//             - verified
//                 2 dispatch user
//                 3 load
//             - unverified
//             - error
//          */
//
//         /*
//         Page callbacks are triggered
//         per use case (isLoggedIn, isVerified, isError)
//          */
//
//         const onActionSuccess = function (){
//             //dispatch something
//             //call page onLoginSuccess
//             onLoginSuccess({isUserExist:is});
//         }
//
//         const onActionError = function (){
//
//         }
//
//         console.log("api.login calling firebase function")
//
//         let callbacks = {
//             onActionSuccess:onActionSuccess,
//             onActionError:onActionError
//         }
//         let p = adapter.Auth.login(data, callbacks);
//     };
// }
//
// export function resetPassword(data, successCB, errorCB) {
//     return (dispatch) => {
//         api.resetPassword(data, function (success, data, error) {
//             if (success) successCB();
//             else if (error) errorCB(error)
//         });
//     };
// }
//
// export function signOut(successCB, errorCB) {
//     return (dispatch) => {
//
//         let keys = ['user'];
//         AsyncStorage.multiRemove(keys);
//
//         api.signOut(function (success, data, error) {
//             if (success) {
//                 dispatch({type: t.LOGGED_OUT});
//                 successCB();
//             }else if (error) errorCB(error)
//         });
//     };
// }
//
// export function checkVerify(user, successCB, errorCB){
//     return (dispatch) => {
//         api.checkVerify(user, function(user, success){
//             if(success){
//                 successCB(user);
//             }
//             else{
//                 errorCB(user);
//             }
//         });
//     };
// }
//
// export function checkLoginStatus(callback) {
//     return (dispatch) => {
//         auth.onAuthStateChanged((user) => {
//             let isLoggedIn = (user !== null);
//
//             if (isLoggedIn) {
//                 //get the user object from the Async storage
//                 AsyncStorage.getItem('user', (err, user) => {
//                     if (user === null) isLoggedIn = false //set the loggedIn value to false
//                     else {
//                         dispatch({type: t.LOGGED_IN, data: JSON.parse(user)})
//                         console.log("WOOPS");
//                     }
//
//                     callback(isLoggedIn);
//                 });
//             } else {
//                 dispatch({type: t.LOGGED_OUT});
//                 callback(isLoggedIn);
//             }
//         });
//     };
// }
