import {
    COMPLETE_PROFILE_ERROR,
    FORGOT_PASSWORD_ERROR,
    LOGGED_IN, LOGIN_ERROR,
    LOGGED_OUT,
    REGISTER_ERROR, REGISTER_REQUEST,
    VERIFY_EMAIL_ERROR,
    REFRESH_USER
} from "./actions";


let initialState = {
    isLoggedIn: false,
    user: null,
    loginError: null,
    registerError: null,
    verifyEmailError: null,
    forgotPasswordError: null,
    completeProfileError: null,
    disableForm: false,
    formErrorDate: 0
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {


        case LOGGED_IN:
            return Object.assign({}, state, {
                isLoggedIn: true,
                user: action.data,
                loginError: null
            });

        case LOGGED_OUT:
            return Object.assign({}, state, {
                isLoggedIn: false,
                user: null,
                loginError: null });

        case LOGIN_ERROR:
            return Object.assign({}, state, {
                loginError: action.data,
                formErrorDate: action.date
            });

        case REGISTER_REQUEST:
            return Object.assign({}, state, {
                disableForm: action.disableForm
            });

        case REGISTER_ERROR:
            return Object.assign({}, state, {
                registerError: action.data,
                formErrorDate: action.date
            });

        case FORGOT_PASSWORD_ERROR:
            return Object.assign({}, state, {
                forgotPasswordError: action.data,
                formErrorDate: action.date
            });

        case COMPLETE_PROFILE_ERROR:
            return Object.assign({}, state, {
                completeProfileError: action.data,
                formErrorDate: action.date
            });

        case VERIFY_EMAIL_ERROR:
            return Object.assign({}, state, {
                verifyEmailError: action.data,
                formErrorDate: action.date
            });


        case REFRESH_USER:
            console.log("DISPATCHED: \n",)
            action.user.print()
            return Object.assign({}, state, {
                user: action.user
            });



        default:
            return state;
    }
};

export default authReducer;