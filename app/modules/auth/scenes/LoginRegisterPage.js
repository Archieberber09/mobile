/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with HOW things work.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here

import React from "react";
import {connect} from 'react-redux';
import Carousel from 'react-native-snap-carousel';
import {View} from "react-native";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";


class LoginRegisterPage extends React.Component {


    constructor() {
        super();

    }


    _renderItem ({item, index}) {
        return (
            <View>
                {item}
            </View>
        );
    }

    render () {
        return (
            <Carousel
                ref={(c) => { this._carousel = c; }}
                data={[
                    <LoginPage/>,
                    <RegisterPage/>
                ]}
                renderItem={this._renderItem}
                sliderWidth={350}
                sliderHeight={400}
                itemWidth={350}
                itemHeight={400}
                vertical
                loop
                layout='stack'
                loopClonesPerSide={1}

            />
        );
    }
}


// for easier debugging
// (eg. error indicating you passed a text instead of a function)
// LoginPage.propTypes = {
//     template: PropTypes.func,
// };

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
    // loginError  : state.authReducer.loginError,
});

const mapDispatchToProps = (dispatch) => ({
    // login: bindActionCreators(login, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(LoginRegisterPage);
