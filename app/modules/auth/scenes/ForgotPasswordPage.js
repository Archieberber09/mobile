/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with HOW things work.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here
import {ForgotPassword} from "../scenes_design/ForgotPassword";
import React from "react";
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {resetPassword} from "../actions";
import {Form} from "../../../components";


class ForgotPasswordPage extends React.Component {


    constructor() {
        super();

        this.onSubmitForm = this.onSubmitForm.bind(this);
        this.onResetPasswordSuccess = this.onResetPasswordSuccess.bind(this);
    }

    onSubmitForm(data) {
        this.props.resetPassword(data, this.onResetPasswordSuccess);
        console.log("ForgotPassword onSubmitForm");
    }

    onResetPasswordSuccess(){
        alert("Password reminder sent to your email");
        Actions.pop()
    }

    render() {
        return (
            <ForgotPassword
                formFields = {[
                    {
                        key: "email",
                        type: Form.FIELD_EMAIL,
                        label: "email address",
                        customBackgroundColor:'#EBEBEB'
                    }
                ]}
                error        = {this.props.forgotPasswordError}
                onSubmitForm = {this.onSubmitForm}
                onBackClick  = {() => Actions.pop()}
                highlightError
                collapseValidationError
            />
        );
    }
}


// for easier debugging
// (eg. error indicating you passed a text instead of a function)
// LoginPage.propTypes = {
//     template: PropTypes.func,
// };

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
    forgotPasswordError: state.authReducer.forgotPasswordError,
    uniqueKey   : state.authReducer.formErrorDate
    //needVerify
});

const mapDispatchToProps = (dispatch) => ({
    resetPassword: bindActionCreators(resetPassword, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordPage);
