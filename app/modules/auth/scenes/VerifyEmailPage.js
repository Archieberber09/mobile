/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with HOW things work.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here

import {Login} from "../scenes_design/Login";
import React from "react";
import {Actions} from 'react-native-router-flux';
import {Text, View} from "react-native";
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as authActions from "../actions";
import {VerifyEmail} from "../scenes_design/VerifyEmail";


class VerifyEmailPage extends React.Component {


    constructor() {
        super();

        this.onResendVerify = this.onResendVerify.bind(this);
    }

    onResendVerify() {
        console.log("verifyemail onSubmitForm")
        this.props.sendVerification(this.onResendVerifySuccess)
    }

    onResendVerifySuccess() {
        // if (this.props.user.exists) Actions.Main()
        // else Actions.CompleteProfile()
        alert("Email verification sent!");
        console.log("Accepted")
    }

    onProceedClick(){
        Actions.reset("Auth");
        // Actions.Main();
    }

    render() {

        return (
            <VerifyEmail
                onResendVerifyClick = {this.onResendVerify}
                onProceedClick      = {this.onProceedClick}
            />
        );
    }
}


// for easier debugging
// (eg. error indicating you passed a text instead of a function)
// LoginPage.propTypes = {
//     template: PropTypes.func,
// };

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
    verifyEmailError: state.authReducer.verifyEmailError,
});

const mapDispatchToProps = (dispatch) => ({
    sendVerification: bindActionCreators(authActions.sendVerify, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(VerifyEmailPage);
