/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with HOW things work.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here
import React from "react";
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Welcome} from "../scenes_design/Welcome";

class WelcomePage extends React.Component {

    constructor() {
        super();
    }

    onLoginClick(){
        Actions.Login();
    }

    onRegisterClick(){
        Actions.Register();
    }

    render() {

        return (
            <Welcome
                onLoginClick = {this.onLoginClick}
                onRegisterClick = {this.onRegisterClick}
            />
        );
    }
}


// for easier debugging
// (eg. error indicating you passed a text instead of a function)
// LoginPage.propTypes = {
//     template: PropTypes.func,
// };

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(WelcomePage);
