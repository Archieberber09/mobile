/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with HOW things work.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here

import React from "react";
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {login} from "../actions";
import {CompleteProfile} from "../scenes_design/CompleteProfile";
import {Form} from "../../../components";


class CompleteProfilePage extends React.Component {


    constructor() {
        super();

        this.onSubmitForm = this.onSubmitForm.bind(this);
        this.onCompleteProfileSuccess = this.onCompleteProfileSuccess.bind(this);
    }

    onSubmitForm(data) {
        console.log("CompleteProfilePage onSubmitForm")
    }

    onCompleteProfileSuccess(){

    }

    render() {

        return (
            <CompleteProfile
                formFields = {[
                    {
                        type:Form.FIELD_FULL_NAME,
                        customBackgroundColor:'#E5E5E5'
                    },
                    {
                        type: Form.FIELD_PHONE_NUMBER,
                        customBackgroundColor:'#E0E0E0'
                    },
                ]}
                // onForgotPassword    = {this.onForgotPassword}
                onSubmitForm        = {()=>{}}
                error               = {this.props.loginError}
                onBackClick         = {() => Actions.pop()}
                highlightError
                collapseValidationError
            />
        );
    }
}


// for easier debugging
// (eg. error indicating you passed a text instead of a function)
// LoginPage.propTypes = {
//     template: PropTypes.func,
// };

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
    completeProfileError: state.authReducer.completeProfileError,
    uniqueKey   : state.authReducer.formErrorDate
    //needVerify
});

const mapDispatchToProps = (dispatch) => ({
    complete: bindActionCreators(login, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(CompleteProfilePage);
