/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with HOW things work.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here
import React from "react";
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import * as authActions from "../actions";
import {Form} from "../../../components";
import {Register} from "../scenes_design/Register";

class RegisterPage extends React.Component {

    constructor() {
        super();

        this.onSubmitForm           = this.onSubmitForm.bind(this);
        this.onRegisterSuccess  = this.onRegisterSuccess.bind(this)
    }

    onSubmitForm(data) {
        console.log("LoginPage onSubmitForm")

        this.props.register(data, this.onRegisterSuccess)
    }

    //soon refactor to RNRF <LightBox/>
    onRegisterSuccess() {
        Actions.VerifyEmail()
    }

    render() {

        return (
            <Register
                formFields = {[
                    {
                        type: Form.FIELD_EMAIL,
                        customBackgroundColor:'#EBEBEB'
                    },
                    {
                        type:Form.FIELD_FULL_NAME,
                        customBackgroundColor:'#E5E5E5'
                    },
                    {
                        type: Form.FIELD_PHONE_NUMBER,
                        customBackgroundColor:'#E0E0E0'
                    },
                    {
                        type: Form.FIELD_PASSWORD,
                        secureTextEntry: true,
                        customBackgroundColor:'#D9D9D9'
                    },
                    {
                        type: Form.FIELD_CONFIRM_PASSWORD,
                        secureTextEntry: true,
                        customBackgroundColor:'#D5D5D5'
                    },
                ]}
                formKey     = {this.props.uniqueKey}
                onSubmitForm= {this.onSubmitForm}
                error       = {this.props.registerError}
                onBackClick = {() => Actions.pop()}
                collapseValidationError
                highlightError
            />
        );
    }
}


// for easier debugging
// (eg. error indicating you passed a text instead of a function)
// LoginPage.propTypes = {
//     template: PropTypes.func,
// };

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
    registerError: state.authReducer.registerError,
    uniqueKey   : state.authReducer.formErrorDate
});

const mapDispatchToProps = (dispatch) => ({
    register: bindActionCreators(authActions.register, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
