/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with how things WORK.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here
import React from "react";
import { connect } from "react-redux";
import { Settings } from "../scenes_design/Settings";
import { Actions } from "react-native-router-flux";
import { bindActionCreators } from "redux";
import { logout } from "../../auth/actions";
// import PropTypes from 'prop-types';

class SettingsPage extends React.Component {
  constructor(props) {
    super(props);

    this.onLogOutClick = this.onLogOutClick.bind(this);
  }

  componentDidMount() {}

  onLogOutClick() {
    this.props.logout(this.props.user, this.onLogoutSuccess);
  }

  onLogoutSuccess() {
    Actions.reset("Auth");
  }

  render() {
    return (
      <Settings
        name={this.props.user.getName()}
        onLogOutClick={this.onLogOutClick}
      />
    );
  }
}

/* UNCOMMENT and EDIT*/

// use this so you dont hae to debug only to
// know you passed a text instead of a function
// SettingsPage.propTypes = {
// };

// map reducer states to this state for ez sync
const mapStateToProps = state => ({
  user: state.authReducer.user
});

const mapDispatchToProps = dispatch => ({
  logout: bindActionCreators(logout, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
