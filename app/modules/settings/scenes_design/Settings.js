/*
This is a Presentation
Also called Dumb/Stateless Component.

ONLY used for RECEIVING PROPS and STYLING COMPONENTS
AVOID using logic here UNLESS it's for animation/display purposes

Presentation:
1. Are concerned with how things LOOK.
2. May contain both presentational and container components** inside, and usually have some DOM markup and styles of their own.
3. Often allow containment via this.props.children.
4. Have no dependencies on the rest of the app, such as Flux actions or stores.
5. Don’t specify how the data is loaded or mutated.
6. Receive data and callbacks exclusively via props.
7. Rarely have their own state (when they do, it’s UI state rather than data).
8. Are written as functional components unless they need state, lifecycle hooks, or performance optimizations.

Examples: Page, Sidebar, Story, UserInfo, List.

How to test a Container component:
> Use this component in any modules with ease
 */

// Add imports here
import React from "react";

import b from "../../../styles/base";
import {
  Image,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView
} from "react-native";
import { Button, Icon } from "react-native-elements";
import assets from "../../../assets";

export const Settings = props => {
  return (
    <SafeAreaView style={b.container}>
      <View style={[b.page, b.mainSection]}>
        <View style={[b.b, b.container, { flex: 0, marginVertical: 15 }]}>
          <Text style={[b.b, b.title]}>SETTINGS</Text>
        </View>

        <View
          style={[
            b.b,
            b.section,
            {
              marginTop: 7,
              justifyContent: "flex-start",
              flexDirection: "row"
            }
          ]}
        >
          {/* <View style={[b.b]}>
            <Image style={{ marginLeft: -5 }} source={assets.AVATAR} />
          </View> */}
          <View style={[b.b, { marginLeft: 15 }]}>
            <Text
              style={[{ fontSize: 20, fontWeight: "bold", color: "#FAFAFA" }]}
            >
              {props.name}
            </Text>
            <Text style={[{ fontSize: 10, color: "#fafafa" }]}>
              view profile
            </Text>
          </View>
        </View>

        <View style={[b.b, { marginTop: 42, width: "100%" }]}>
          <TouchableOpacity
            style={[
              {
                marginBottom: 17,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }
            ]}
          >
            <Text
              style={[
                b.b,
                { color: "#FAFAFA", fontSize: 16, fontWeight: "bold" }
              ]}
            >
              Change name
            </Text>
            <Icon size={15} name="chevron-right" color="#FAFAFA" style={{color: "#fafafa", backgroundColor: "#fafafa"}} type="entypo" />
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              {
                marginBottom: 17,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }
            ]}
          >
            <Text
              style={[
                b.b,
                { color: "#FAFAFA", fontSize: 16, fontWeight: "bold" }
              ]}
            >
              Change e-mail
            </Text>
            <Icon size={15} name="chevron-right" color="#FAFAFA" style={{color: "#fafafa", backgroundColor: "#fafafa"}} type="entypo" />
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              {
                marginBottom: 17,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }
            ]}
          >
            <Text
              style={[
                b.b,
                { color: "#FAFAFA", fontSize: 16, fontWeight: "bold" }
              ]}
            >
              Change password
            </Text>
            <Icon size={15} name="chevron-right" color="#FAFAFA" style={{color: "#fafafa", backgroundColor: "#fafafa"}} type="entypo" />
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              {
                marginBottom: 17,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }
            ]}
          >
            <Text
              style={[
                b.b,
                { color: "#FAFAFA", fontSize: 16, fontWeight: "bold" }
              ]}
            >
              Change GCash details
            </Text>
            <Icon size={15} name="chevron-right" color="#FAFAFA" style={{color: "#fafafa", backgroundColor: "#fafafa"}} type="entypo" />
          </TouchableOpacity>
        </View>

        <View>
          <Button
            containerStyle={[{ marginTop: 27 }, b.mainButtonContainer]}
            buttonStyle={[{}, b.mainButton]}
            titleStyle={[b.mainButtonText]}
            title="LOG OUT"
            onPress={props.onLogOutClick}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};
