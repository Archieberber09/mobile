import React from 'react';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux'

import {ConfirmedScan} from "../scenes_design/ConfirmedScan";

class ConfirmedScanPage extends React.Component {

    constructor(props){
        super(props);
    }

    onHomeClick(){
        Actions.refresh(Actions.Home)
        Actions.Home()
    }

    render() {
        return (
            <ConfirmedScan
                points  = {this.props.transaction.getPoints()}
                onHomeClick = {this.onHomeClick}
            />
        );
    }
}



const mapStateToProps = (state, ownProps) => ({
    transaction : state.scanReducer.currentTansaction,
});

function mapDispatchToProps(dispatch) {
    return {
        // collectPoints    : bindActionCreators(collectPoints, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmedScanPage);