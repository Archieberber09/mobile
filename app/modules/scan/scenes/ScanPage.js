import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {View} from "react-native";
import {bindActionCreators} from "redux";
import {Actions} from 'react-native-router-flux'

import API from "../../../api/index"
import {Scan} from "../scenes_design/Scan";
import * as authActions from "../actions";
import {collectPoints} from "../actions";

class ScanPage extends React.Component {

    constructor(props){
        super(props);
        // this.props.getCountDownTime();
        this.state = {curScene: null}

        this.onQRCodeRead = this.onQRCodeRead.bind(this)
    }

    componentDidMount() {
        setInterval(() => {
            if(this.state.curScene === null && Actions.currentScene === "_Scan"){
                this.setState({curScene: Actions.currentScene});
            }
            else if(this.state.curScene === "_Scan" && Actions.currentScene !== "_Scan"){
                this.setState({curScene: null});
            }
        },100)
    }


    componentWillMount() {
        // this.didFocusListener = this.props.navigation.addListener(
        //     'willFocus',()=>
        //     this.setState({ focusedScreen: true })
        // );
    }

    componentWillUnmount() {
        // this.didFocusListener.remove();
        // this.setState({ focusedScreen: false })
    }


    onQRCodeRead(data){
        this.props.collectPoints(this.props.user, data.data, this.onCollectPointsSuccess, this.onCollectPointsError);
    }

    onCollectPointsSuccess(){
        Actions.ConfirmedScan()
    }

    onCollectPointsError(){
        Actions.RejectedScan()
    }

    render() {
        return (
                this.state.curScene ?
            <Scan
                onQRCodeRead={this.onQRCodeRead}
            />
            :<View/>
    );
    }
}



ScanPage.propTypes = {
    isLoading:          PropTypes.bool,
    countdownTime:      PropTypes.number,
    getCountDownTime:   PropTypes.func
};

const mapStateToProps = (state, ownProps) => ({
    // isLoading       :  state.homeReducer.isLoading,
    user            :  state.authReducer.user,
});

function mapDispatchToProps(dispatch) {
    return {
        collectPoints    : bindActionCreators(collectPoints, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ScanPage);