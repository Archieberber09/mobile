import React from 'react';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux'

import {RejectedScan} from "../scenes_design/RejectedScan";

class RejectedScanPage extends React.Component {

    constructor(props){
        super(props);
    }

    onHomeClick(){
        Actions.refresh(Actions.Home)
        Actions.Home()
    }

    render() {
        return (
            <RejectedScan
                message  = {this.props.error}
                onHomeClick = {this.onHomeClick}
            />
        );
    }
}



const mapStateToProps = (state, ownProps) => ({
    error : state.scanReducer.collectPointsError,
});

function mapDispatchToProps(dispatch) {
    return {
        // collectPoints    : bindActionCreators(collectPoints, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RejectedScanPage);