import {COLLECT_POINTS_REQUEST, COLLECT_POINTS_SUCCESS, COLLECT_POINTS_ERROR} from "./actions";

let initialState = {
    currentTansaction: null,
    collectPointsError: null
};

const ScanReducer = (state = initialState, action) => {
    switch (action.type) {

        case COLLECT_POINTS_REQUEST: {
            return state;
        }

        case COLLECT_POINTS_SUCCESS: {
            return {...state, currentTansaction: action.currentTansaction};
        }

        case COLLECT_POINTS_ERROR: {
            return {...state, collectPointsError: action.error};
        }

        default:
            return state;
    }
};

export default ScanReducer;