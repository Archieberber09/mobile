import * as adapter from "../../adapter";
import {LOGIN_ERROR, REFRESH_USER} from "../auth/actions";
import {GET_TRANSACTIONS_SUCCESS} from "../home/actions";


export const COLLECT_POINTS_REQUEST        = 'SCAN/GENERATE_POINTS_REQUEST'
export const COLLECT_POINTS_SUCCESS        = 'SCAN/GENERATE_POINTS'
export const COLLECT_POINTS_ERROR          = 'SCAN/GENERATE_POINTS'

export function collectPoints(user, url, onCollectPointsSuccess, onCollectPointsError) {
    return (dispatch) => {
        dispatch({type:COLLECT_POINTS_REQUEST})

        adapter.Database.mCollectPoints(user, url)
            .then(([user, transaction]) => {
                if(transaction){
                    dispatch({type:COLLECT_POINTS_SUCCESS, currentTansaction: transaction})
                    dispatch({type: REFRESH_USER, user:user})
                    onCollectPointsSuccess();
                }
                else return Promise.reject({message: 'Invalid link. Please try again'})

                return adapter.Database.mgetTransactions(user)
            })
            .then((transactions)=>{
                dispatch({type: GET_TRANSACTIONS_SUCCESS, data: transactions})
            })
            .catch((error)=>{

                if (error.message){
                    console.log("dispatched error is: "+error.message)
                    dispatch({type: COLLECT_POINTS_ERROR, error: error.message})
                }
                else{
                    console.log(error)
                    dispatch({type: COLLECT_POINTS_ERROR, error: error.toString()})
                }

                onCollectPointsError();
            });


    };
}