import React from "react";
import { Text, TouchableOpacity, View, SafeAreaView } from "react-native";

import b from "../../../styles/base";
import { Button } from "react-native-elements";
import QRCodeScanner from "react-native-qrcode-scanner";

import { QRScanner } from "../../../components";

export const Scan = props => {
  return (
    <SafeAreaView style={b.container}>
      <View style={[b.page]}>
        <QRScanner
          showMarker
          reactivate
          onRead={props.onQRCodeRead}
          topContent={
            <Text style={[b.b, b.title, { paddingVertical: 15 }]}>SCAN</Text>
          }
          bottomContent={
            <Text
              style={[
                b.b,
                b.title,
                b.authTitle,
                { fontSize: 16, paddingHorizontal: 20 }
              ]}
            >
              Place the camera on the QR Code
            </Text>
          }
          style={[
            b.h,
            {
              flex: 0,
              height: 500,
              width: "100%",
              justifyContent: "space-between",
              alignItems: "stretch"
            }
          ]}
          cameraStyle={[
            b.h,
            {
              height: 350,
              width: "100%",
              flex: 0,
              borderRadius: 15,
              overflow: "hidden"
            }
          ]}
          topContentStyle={[
            b.wrapper,
            b.b,
            { backgroundColor: "transparent", flex: 0, zIndex: 10 }
          ]}
          bottomContentStyle={[
            b.wrapper,
            b.b,
            { backgroundColor: "transparent", height: 50, flex: 0 }
          ]}
        />
      </View>
    </SafeAreaView>
  );
};
/*
<Button
                                containerStyle={[{},b.mainButtonContainer]}
                                buttonStyle={[{},b.mainButton]}
                                titleStyle={[b.mainButtonText]}
                                title='SCAN QR CODE'
                                onPress={()=>{props.onRegisterClicked()}}
                            />
 */
