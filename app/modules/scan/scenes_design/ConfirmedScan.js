import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";

import b from '../../../styles/base'
import {Button} from "react-native-elements";

export const ConfirmedScan = (props) => {
    return (
        <View style={[b.page]}><View style={[b.container]}>
            <View style={[b.b,b.authTitleContainer]}>
                    <Text style={[b.b,b.title,b.authTitle,{fontSize:18, paddingHorizontal:20}]}>
                        YOUR ACCOUNT HAS BEEN REWARDED
                    </Text>
                    <Text style={[b.b,b.title,b.authTitle,{fontSize:16,fontWeight:'normal', paddingHorizontal:20}]}>
                        {props.points} points are automatically added to the score
                    </Text>
                </View>
                <View style={[b.h,b.wrapper,{marginBottom:10}]}>
                    <View>
                        <Button
                            containerStyle={[{},b.authButtonContainer]}
                            buttonStyle={[{},b.authButton]}
                            titleStyle={[b.authButtonText]}
                            title='BACK TO HOME'
                            onPress={props.onHomeClick}
                        />
                    </View>
                </View></View>
        </View>
    );
}
/*
<Button
                                containerStyle={[{},b.mainButtonContainer]}
                                buttonStyle={[{},b.mainButton]}
                                titleStyle={[b.mainButtonText]}
                                title='SCAN QR CODE'
                                onPress={()=>{props.onRegisterClicked()}}
                            />
 */