/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with HOW things work.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here

import {Cashout} from "../scenes_design/Cashout";
import React from "react";
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {login} from "../actions";
import {Text, View} from "react-native";

// import { LoginManager, AccessToken } from "react-native-fbsdk";
// import { GoogleSignin, statusCodes } from 'react-native-google-signin';

class LoginPage extends React.Component {


    constructor() {
        super();
        // GoogleSignin.configure({webClientId:'641753502614-0i0a8plpqqbr1fgv4mg4vfnamj47g7en.apps.googleusercontent.com'});

        this.onSubmitForm = this.onSubmitForm.bind(this);
        this.onLoginSuccess = this.onLoginSuccess.bind(this);
        // this.onLoginError = this.onLoginError.bind(this)
    }

    onSubmitForm(formData) {
        console.log("LoginPage onSubmitForm")

        this.props.login(formData, this.onLoginSuccess)
    }



    //conditions for success: verified
    //might need global config for route propagation
    onLoginSuccess() {
        // let isUserExist = this.props.user.exists;
        // let isVerified  = this.props.user.emailVerified;

        // (isUserExist)
        //     ? (isVerified)
        //        ?  Actions.Main()
                // : Actions.VerifyEmail()
            // : Actions.CompleteProfile()
        console.log("onLoginSuccess")
        console.log(this.props.user)
        Actions.Main()
    }

    onRegisterClicked(){
        Actions.Register();
    }

    onForgotPasswordClicked(){
        Actions.ForgotPassword();
    }



    render() {

        return (
            <Cashout
                formFields = {[
                    {
                        type: "email",
                    },
                    {
                        type: "password",
                        secureTextEntry: true,
                        // customBackgroundColor:'#E5E5E5'
                    },
                ]}

                formKey                 = {this.props.uniqueKey}
                onSubmitForm            = {this.onSubmitForm}
                error                   = {this.props.loginError}
                onFBLoginClick          = {this.onFBLoginClick}
                onGoogleLoginClick      = {this.onGoogleLoginClick}
                onRegisterClicked       = {this.onRegisterClicked}
                onForgotPasswordClicked = {this.onForgotPasswordClicked}
                onBackClick             = {() => Actions.pop()}
                highlightError
                collapseValidationError
            />
        );
    }
}


// for easier debugging
// (eg. error indicating you passed a text instead of a function)
// LoginPage.propTypes = {
//     template: PropTypes.func,
// };

// map reducer states to this state for ez sync
const mapStateToProps = (state) => ({
    loginError  : state.authReducer.loginError,
    user        : state.authReducer.user,
    uniqueKey   : state.authReducer.formErrorDate
    //needVerify
});

const mapDispatchToProps = (dispatch) => ({
    login: bindActionCreators(login, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
