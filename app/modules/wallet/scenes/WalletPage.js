/*
This is a Container
Also called Smart/Stateful Component.

ONLY used for FETCHING/PASSING DATA and RENDERING COMPONENTS
AVOID styling components here

Containers:
1. Are concerned with how things WORK.
2. May contain both presentational and container components.
3. Provide the data and behavior to presentational or other container components.
4. Call Redux actions and provide these as callbacks to the presentational components.
5. Are often stateful, as they tend to serve as data sources.

Examples: UserPage, FollowersSidebar, StoryContainer, FollowedUserList.

How to test a Container component:
> Change components from render() with ease.
 */

// Add imports here
import React from "react";
import { connect } from "react-redux";
import { Wallet } from "../scenes_design/Wallet";
import TransactionItem from "../../home/components/TransactionItem";
import { createCashout } from "../actions";
import {
  StyleSheet,
  View,
  Text,
  Button,
  TextInput,
  SafeAreaView
} from "react-native";
import Modal from "react-native-modal";
import b from "../../../styles/base";
// import PropTypes from 'prop-types';

class WalletPage extends React.Component {
  state = {
    cashoutVisibility: false,
    successVisibility: false,
    errorVisibility: false,
    amount: 0
  };
  constructor(props) {
    super(props);
    this.handleCashout = this.handleCashout.bind(this);
    this.toggleCashoutModal = this.toggleCashoutModal.bind(this);
    this.toggleSuccessModal = this.toggleSuccessModal.bind(this);
    this.toggleErrorModal = this.toggleErrorModal.bind(this);
  }

  componentDidMount() {}

  renderTransactionItem({ item, index }) {
    return <TransactionItem content={item} />;
  }

  handleCashout() {
    if (this.state.amount > this.props.user.getPoints()) {
      this.toggleCashoutModal();
      this.toggleErrorModal();
    } else {
      let data = {
        customer_id: this.props.user.getId(),
        amount: this.state.amount
      };
      createCashout(this.props.user.getAccessToken(), data);
    }
  }
  toggleCashoutModal() {
    console.log("toggle cashout");
    this.setState({
      ...this.state,
      cashoutVisibility: !this.state.cashoutVisibility
    });
    console.log(this.state.cashoutVisibility);
  }
  toggleSuccessModal() {
    this.setState({
      ...this.state,
      successVisibility: !this.state.successVisibility
    });
  }
  toggleErrorModal() {
    this.setState({
      ...this.state,
      errorVisibility: !this.state.errorVisibility
    });
  }

  render() {
    const styles = StyleSheet.create({
      buttonStyleContainer: {
        flex: 1,
        flexDirection: "row",
        marginHorizontal: 20,
        marginTop: 5
      }
    });
    return (
      <React.Fragment>
        <Wallet
          points={this.props.user.getPoints()}
          transactions={this.props.transactions}
          toggleCashout={this.toggleCashoutModal}
          toggleSuccess={this.toggleSuccessModal}
          toggleError={this.toggleErrorModal}
          cashoutVisibility={this.state.cashoutVisibility} 
          successVisibility={this.state.successVisibility} 
          errorVisibility={this.state.errorVisibility} 
          // announcementListKeyExtractor = {(item,index) => item.index.toString()}
          renderTransactionItem={this.renderTransactionItem}
        />      

        <Modal isVisible={this.state.successVisibility}>
          <View style={{ flex: 1 }}>
            <Text>
              You have successfully requested for a cashout! Please wait 2-3
              business days while our admin processes your payment
            </Text>
            <Button title="Close" onPress={this.toggleSuccessModal} />
          </View>
        </Modal>
        <Modal isVisible={this.state.errorVisibility}>
          <View style={{ flex: 1 }}>
            <Text>
              Oops.. It seems an error was encountered while trying to process
              your request, please try again later!
            </Text>
            <Button title="Close" onPress={this.toggleErrorModal} />
          </View>
        </Modal>
      </React.Fragment>
    );
  }
}
// use this so you dont hae to debug only to
// know you passed a text instead of a function
// LoginPage.propTypes = {
//     template: PropTypes.func,
//
// };

// map reducer states to this state for ez sync
const mapStateToProps = state => ({
  user: state.authReducer.user,
  transactions: state.homeReducer.transactions
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(WalletPage);
