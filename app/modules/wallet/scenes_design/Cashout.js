import React from 'react';
import {View, Text, ImageBackground, Image, Dimensions} from "react-native";
import {Button} from 'react-native-elements'
import {Actions} from 'react-native-router-flux';

import {Form} from "../../../components";
import b from "../../../styles/base";
import assets from "../../../assets";



export const Cashout = (props) => {
    return (
        <ImageBackground
            source={assets.BACKGROUND_IMAGE}
            style={[b.b,b.container,b.background]}
            resizeMode='stretch'
        >
            <View style={[b.b,{alignItems:'center', width:'100%'}]}>
                <Image
                    source={assets.LOGO}
                    style={[b.b,{width:120, height:120,resizeMode:'contain'}]}
                />
                <Text style={[{fontSize:15, color:'#E4E4E4'}]}>
                    TRANSFER TO GCASH
                </Text>
            </View>
            <Form uniqueKey           = {props.formKey}
                  fields              = {props.formFields}
                  buttonTitle         = "SUBMIT"
                  onSubmitForm    = {props.onSubmitForm}
                  error           = {props.error}
                  showLabel       = {props.showLabel}
                  highlightError  = {props.highlightError}
                  collapseValidationError={props.collapseValidationError}
                  placeholderTextColor = '#FAFAFA'

                  style={[b.b, b.authForm]}
                  fieldStyle={[b.b, b.authFormField]}
                  inputContainerStyle={[b.b,b.authFormInputContainer]}
                  inputStyle={[b.b,b.authFormInput]}
                  inputContentStyle={[b.b,b.authFormInputContent]}
                  buttonContainerStyle={[b.authFormButtonContainer]}
                  buttonStyle={[b.authFormButton]}
                  buttonTextStyle={[b.authFormButtonText]}
            />
            <View style={[b.h,b.wrapper,{marginBottom:10}]}>
                <View>
                    <Button
                        containerStyle={[{},b.authFormButtonContainer]}
                        buttonStyle={[{},b.authFormButton]}
                        titleStyle={[b.authFormButtonText]}
                        title='CANCEL'
                        onPress={Actions.pop()}
                    />
                </View>
            </View>
            {/*<View style={[b.wrapper]}>*/}
                {/*<View style={[b.authSocialCashoutContainer]}>*/}
                    {/*<Text style={[b.authBottomTextClickable]} onPress={props.onBackClick}>*/}
                        {/*back*/}
                    {/*</Text>*/}
                {/*</View>*/}
            {/*</View>*/}
        </ImageBackground>
    );
}


/*
const form = createForm({config})
const logo = logo
const components = {logo, form}

return (loop through components)



 */