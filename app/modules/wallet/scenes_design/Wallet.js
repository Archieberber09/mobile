/*
This is a Presentation
Also called Dumb/Stateless Component.

ONLY used for RECEIVING PROPS and STYLING COMPONENTS
AVOID using logic here UNLESS it's for animation/display purposes

Presentation:
1. Are concerned with how things LOOK.
2. May contain both presentational and container components** inside, and usually have some DOM markup and styles of their own.
3. Often allow containment via this.props.children.
4. Have no dependencies on the rest of the app, such as Flux actions or stores.
5. Don’t specify how the data is loaded or mutated.
6. Receive data and callbacks exclusively via props.
7. Rarely have their own state (when they do, it’s UI state rather than data).
8. Are written as functional components unless they need state, lifecycle hooks, or performance optimizations.

Examples: Page, Sidebar, Story, UserInfo, List.

How to test a Container component:
> Use this component in any modules with ease
 */

// Add imports here
import React from "react";

import b from "../../../styles/base";
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView
} from "react-native";
import { Icon } from "react-native-elements";
import assets from "../../../assets";
import Modal from "react-native-modal";
export const Wallet = props => {
  return (
    <SafeAreaView style={b.container}>
      <View style={[b.page]}>
        <View style={[b.b, b.container, { flex: 0, marginVertical: 15 }]}>
          <Text style={[b.b, b.title]}>WALLET</Text>
          <View
            style={[
              { flexDirection: "row", marginTop: 12, alignItems: "center" }
            ]}
          >
            <Image
              source={assets.MINI_LOGO}
              // style={[b.b,{width:27, height:24}]}
            />
            <Text>{"  "}</Text>
            <Text style={[b.homeWalletTitle]}>{props.points} points</Text>
          </View>
        </View>
        <View style={[b.b, b.section]}>
          <TouchableOpacity
            onPress={props.toggleCashout}
            style={[
              b.b,
              b.wrapper,
              {
                height: 86,
                width: "72%",
                backgroundColor: "#165780",
                borderRadius: 10
              }
            ]}
          >
            <Image
              source={assets.WITHDRAWAL}
              style={[b.b, { width: 30, height: 30 }]}
            />
            <Text
              style={[b.b, { marginTop: 8, fontSize: 10, color: "#FAFAFA" }]}
            >
              TRANSFER FUNDS TO GCASH
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={[
            b.b,
            b.homeTransactionsContainer,
            { width: "100%", height: "53%" }
          ]}
        >
          <View style={[b.b, b.wrapper, b.mainSubContainerTitle]}>
            <Text style={[b.b, b.homeSubtitle]}>RECENT POINTS COLLECTION</Text>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={props.transactions}
            renderItem={props.renderTransactionItem}
            // ListEmptyComponent  = {props.listEmptyComponent}
            initialNumToRender={5}
            // keyExtractor        = {props.announcementListKeyExtractor}

            style={[{ marginTop: 5 }]}
            contentContainerStyle={[b.b]}
          />
        </View>
        <Modal isVisible={props.cashoutVisibility} coverScreen={true} hasBackdrop={true} >
          <View style={{ flex: 1 }}>
            <Text>Please enter your requested cashout amount:</Text>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};
