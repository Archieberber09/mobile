import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { Home } from "../scenes_design/Home";
// import {calculateEventCountdownTime} from "../actions";
import { bindActionCreators } from "redux";
import { SplashScreen } from "../../../components/SplashScreen";
import AnnouncementItem from "../components/AnnouncementItem";
import TransactionItem from "../components/TransactionItem";
import { Text, View } from "react-native";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    // this.props.getCountDownTime();
    //this.props.subscribeListeners();
  }

  componentDidMount() {
    //API.Moengage.start();
  }

  componentWillUnmount() {
    //this.props.unsubscribeListeners();
  }

  renderAnnouncementItem({ item, index }) {
    return <AnnouncementItem content={item} />;
  }

  renderTransactionItem({ item, index }) {
    return <TransactionItem content={item} />;
  }

  _listEmptyComponent = () => {
    return (
      <View>
        <Text style={{ color: "#FAFAFA", marginTop: 30 }}>
          There are no announcements to show
        </Text>
      </View>
    );
  };

  render() {
    if (this.props.isLoading) {
      return <SplashScreen />;
    } else {
      return (
        <Home
          points={this.props.user.getPoints()}
          announcements={this.props.announcements}
          transactions={this.props.transactions}
          // announcementListKeyExtractor = {(item,index) => item.index.toString()}

          renderAnnouncementItem={this.renderAnnouncementItem}
          renderTransactionItem={this.renderTransactionItem}
          listEmptyComponent={this._listEmptyComponent}
        />
      );
    }
  }
}

HomePage.propTypes = {
  isLoading: PropTypes.bool,
  countdownTime: PropTypes.number,
  getCountDownTime: PropTypes.func
};

const mapStateToProps = (state, ownProps) => ({
  isLoading: state.homeReducer.isLoading,
  countdownTime: state.homeReducer.countdownTime,
  user: state.authReducer.user,
  announcements: state.homeReducer.announcements,
  transactions: state.homeReducer.transactions
});

function mapDispatchToProps(dispatch) {
  return {
    // getCountDownTime    : bindActionCreators(calculateEventCountdownTime, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
