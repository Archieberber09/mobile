// import React from 'react';
// import {connect} from 'react-redux';
// import PropTypes from 'prop-types';
//
// import {Home} from '../scenes_design/Home'
// import {calculateEventCountdownTime, subscribeListeners, testOnMount, unsubscribeListeners} from "../actions";
// import {bindActionCreators} from "redux";
// import {SplashScreen} from "../../../components/SplashScreen";
//
// import API from "../../../api/index"
//
// class HomePage extends React.Component {
//
//     constructor(props){
//         super(props);
//         this.props.getCountDownTime();
//         //this.props.subscribeListeners();
//     }
//
//     componentDidMount(){
//         //API.Moengage.start();
//     }
//
//     componentWillUnmount(){
//         //this.props.unsubscribeListeners();
//     }
//
//     render() {
//         if (this.props.isLoading) {
//             return (
//                 <SplashScreen/>
//
//             );
//         }
//         else{
//             return (
//                 <Home
//                     countdown_until = {this.props.countdownTime}
//                     countdown_onFinish={() => alert('overrriden')}
//                 />
//             );}
//     }
// }
//
//
//
// HomePage.propTypes = {
//     isLoading:          PropTypes.bool,
//     countdownTime:      PropTypes.number,
//     getCountDownTime:   PropTypes.func
// };
//
// const mapStateToProps = (state, ownProps) => ({
//     isLoading:      state.homeReducer.isLoading,
//     countdownTime:  state.homeReducer.countdownTime
// });
//
// function mapDispatchToProps(dispatch) {
//     return {
//         getCountDownTime    : bindActionCreators(calculateEventCountdownTime, dispatch),
//         subscribeListeners  : bindActionCreators(subscribeListeners, dispatch),
//         unsubscribeListeners: bindActionCreators(unsubscribeListeners, dispatch),
//         testOnMount         : bindActionCreators(testOnMount, dispatch),
//     };
// }
//
// export default connect(mapStateToProps, mapDispatchToProps)(HomePage);