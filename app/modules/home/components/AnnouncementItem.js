import React from 'react';
import {Text, View} from "react-native";
import b from "../../../styles/base";


export default class AnnouncementItem extends React.Component {


    render(){
        return(
            <View style={[
                {height:'93%',width:110,marginRight:20,
                    elevation:1,
                    borderRadius:5,
                    shadowColor: "#111",
                    shadowRadius: 5,
                    shadowOpacity: 0.5,
                    shadowOffset: {width: 10,height: 10 }}]}>
                <View style={[{height:'8%', backgroundColor:'#F4F4F4'}]}>

                </View>
                <View style={[{height:'92%'}]}>
                    <Text>
                        {this.props.content.key}
                    </Text>
                </View>

            </View>
        );
    }
}