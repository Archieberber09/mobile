import React from 'react';
import {Text, View} from "react-native";
import b from "../../../styles/base";


export default class CashoutItem extends React.Component {


    render(){
        return(
            <View style={[
                {flexDirection:'row',height:61,width:'100%',marginBottom:10,
                    elevation:1,
                    borderRadius:5,
                    shadowColor: "#111",
                    shadowRadius: 5,
                    shadowOpacity: 0.5,
                    shadowOffset: {width: 10,height: 10 }}]}>
                <View style={[{width:'3%', backgroundColor:'#628EAA'}]}>

                </View>
                <View style={[b.b,{width:'97%',justifyContent:'center', backgroundColor:'#10284D'}]}>
                    <Text style={[{color:'#FAFAFA', marginLeft:10}]}>
                        amount: {this.props.content.weight} status: {this.props.content.points}
                    </Text>
                </View>
            </View>
        );
    }
}