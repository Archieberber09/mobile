import React from "react";
import {
  FlatList,
  Image,
  ImageBackground,
  Text,
  View,
  SafeAreaView
} from "react-native";

import b from "../../../styles/base";
import { Icon } from "react-native-elements";
import assets from "../../../assets";

export const Home = props => {
  return (
    <SafeAreaView style={b.container}>
      <View style={[b.page, { justifyContent: "flex-start" }]}>
        <View style={[b.b, b.container, { flex: 0, marginVertical: 15 }]}>
          <Text style={[b.b, b.title]}>DASHBOARD</Text>
          <View
            style={[
              b.b,
              { flexDirection: "row", marginTop: 12, alignItems: "center" }
            ]}
          >
            <Image
              source={assets.MINI_LOGO}
              style={[b.b, { width: 27, height: 24 }]}
            />
            <Text>{"  "}</Text>
            <Text style={[b.homeWalletTitle]}>
              {props.points} points
            </Text>
          </View>
        </View>
        <View style={[b.b, b.wrapper, b.homeAnnouncementsContainer]}>
          <View style={[b.b, b.wrapper, b.mainSubContainerTitle]}>
            <Text style={[b.b, b.homeSubtitle]}>ANNOUNCEMENTS</Text>
          </View>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={props.announcements}
            renderItem={props.renderAnnouncementItem}
            ListEmptyComponent={props.listEmptyComponent}
            initialNumToRender={5}
            keyExtractor={props.announcementListKeyExtractor}
            style={[{ marginTop: 5 }]}
            contentContainerStyle={[b.b]}
          />
        </View>
        <View style={[b.b, b.wrapper, b.homeTransactionsContainer]}>
          <View style={[b.b, b.wrapper, b.mainSubContainerTitle]}>
            <Text style={[b.b, b.homeSubtitle]}>RECENT TRANSACTIONS</Text>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={props.transactions}
            renderItem={props.renderTransactionItem}
            // ListEmptyComponent  = {props.listEmptyComponent}
            initialNumToRender={5}
            // keyExtractor        = {props.announcementListKeyExtractor}

            style={[{ marginTop: 5 }]}
            contentContainerStyle={[b.b]}
          />
          {/*<View style={[b.b,b.wrapper,b.mainSubContainerBottomTitle]}>*/}
          {/*<Text style={[b.b,b.homeTextLink]}>*/}
          {/*see more*/}
          {/*</Text>*/}
          {/*</View>*/}
        </View>
      </View>
    </SafeAreaView>
  );
};
