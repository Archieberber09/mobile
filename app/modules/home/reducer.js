import {
    GET_COUNTDOWN_REQUEST,
    GET_COUNTDOWN_SUCCESS, GET_TRANSACTIONS_SUCCESS, GET_WALLET_SUCCESS,
} from "./actions";


let initialState = {
    isLoading: false,
    countdownTime: null,

    announcements: [],
    transactions: [],
};

const homeReducer = (state = initialState, action) => {
    switch (action.type) {

        case GET_COUNTDOWN_REQUEST: {
            console.log("get_countdown_request");
            const time = state.countdownTime;

            //show loading signal
            if (time) return {...state, isLoading: true}

            return state;
        }

        case GET_WALLET_SUCCESS:
            return {...state, user: action.data};

        case GET_TRANSACTIONS_SUCCESS:
            return Object.assign({}, state, {
                transactions: action.data.reverse()
                //refactor this ^
            });

        case GET_COUNTDOWN_SUCCESS:
            console.log("get_countdown_success" + action.timeDifferenceInSeconds);

            return {...state,
                countdownTime: action.timeDifferenceInSeconds,
                isLoading: false
            };

        default:
            return state;
    }
};

export default homeReducer;