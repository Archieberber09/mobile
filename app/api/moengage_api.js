/*
import {NativeEventEmitter, NativeModules} from 'react-native';

import moeManager from "react-native-moengage";

const { MoEReactBridge } = NativeModules;
const MoEReactBridgeEmitter = new NativeEventEmitter(MoEReactBridge);
let subscriptionNotification, subscriptionInAppShown,
    subscriptionInAppClicked, subscriptionEventTriggeredSelfHandledInApp;

export function unsubscribeListeners(){
    console.log('unsubscribing listeners')
    // subscriptionNotification.remove();
    // subscriptionInAppShown.remove();
    // subscriptionInAppClicked.remove();
    // subscriptionEventTriggeredSelfHandledInApp.remove();

}

export function subscribeNotification(){
    console.log("subscribe");
    subscriptionNotification = MoEReactBridgeEmitter.addListener(
        'notificationClicked',
        (connectionInfo) => {
            console.log("subscribe listener triggered");
            console.log(connectionInfo);
        }
    );
}

export function subscribeInAppShown(){
    subscriptionInAppShown = MoEReactBridgeEmitter.addListener(
        'inAppShown',
        (connectionInfo) => {
            console.log("subscribe listener triggered");
            console.log(connectionInfo);
        }
    );
}

export function subscribeInAppClick(){
    subscriptionInAppClicked = MoEReactBridgeEmitter.addListener(
        'inAppClicked',
        (connectionInfo) => {console.log(connectionInfo);console.log("boi");}
    );
}

export function subscribeEventTriggeredSelfHandledInApp(){
    subscriptionEventTriggeredSelfHandledInApp = MoEReactBridgeEmitter.addListener(
        'eventTriggeredSelfHandledInApp',
        (connectionInfo) => {console.log(connectionInfo);}
    );
}

export function start(){
    //For Debugging
    moeManager.setLogLevel(1);

    //Tracking Event
    moeManager.trackEvent("Purchase", {"quantity":1, "product":"iPhone", "currency":"dollar", "price":699, "new_item" : true});
    moeManager.trackEvent("testEvent",{"attribute1":"value"});
    moeManager.trackEvent("testEvent",{"attribute2":321});
    moeManager.trackEvent("Univweek",{"attribute3":true});
    moeManager.trackEvent("testEvent",{"attribute4":123});

    //User Attribute
    moeManager.setUserUniqueID("jdoe@gmail.com");
    moeManager.setUserName("James");
    moeManager.setUserFirstName("John");
    moeManager.setUserLastName("Doe");
    moeManager.setUserEmailID("jdoe@gmail.com");
    moeManager.setUserContactNumber("9473532355");
    moeManager.setUserBirthday("05/05/1991");
    moeManager.setUserGender("Male"); //OR Female

    // moeManager.showInApp()
}
*/