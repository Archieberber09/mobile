const axios = require("axios");

const COOP_WEB_URL =
  "http://192.168.1.7:8000/api/";

  const LOGIN_URL = COOP_WEB_URL + "auth/login";
  const LOGOUT_URL = COOP_WEB_URL + "auth/logout";
  const SIGNUP_URL = COOP_WEB_URL + "auth/signup";
  const USER_URL = COOP_WEB_URL + "auth/user";
  const WALLET_URL = COOP_WEB_URL + "auth/wallet";
  const TRANSACTIONS_URL = COOP_WEB_URL + "auth/orders";
  const CASHOUTS_URL = COOP_WEB_URL + "cashouts";

const DEFAULT_HEADER = {
  headers: {
    "Content-Type": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    "Accept-Encoding": "gzip, deflate",
    Connection: "keep-alive"
  }
};

//Sign the user in with their email and password
export function login(email, password, { onSuccess, onError }) {
  let body = {
    email: email,
    password: password,
    remember_me: true
  };
  console.log(body);

  return axios
    .post(LOGIN_URL, body)
    .then(onSuccess)
    .catch(onError);
}

export function logout(tokenType, accessToken, { onSuccess, onError }) {
  DEFAULT_HEADER.headers.Authorization = tokenType + " " + accessToken;
  return axios
    .get(LOGOUT_URL, DEFAULT_HEADER)
    .then(onSuccess)
    .catch(onError);
}

export function register(
  name,
  email,
  password,
  number,
  { onSuccess, onError }
) {
  let body = {
    name: name,
    email: email,
    password: password,
    password_confirmation: password,
    number: number,
    remember_me: true
  };
  console.log(body);

  return axios
    .post(SIGNUP_URL, body)
    .then(onSuccess)
    .catch(onError);
}

export function sendEmailVerification({ onSuccess, onError }) {
  // return auth.currentUser.sendEmailVerification()
  //     .then(onSuccess)
  //     .catch(onError);
}

export function resetPassword(email, { onSuccess, onError }) {
  // return auth.sendPasswordResetEmail(email)
  //     .then(onSuccess)
  //     .catch(onError);
}

//Create the user object in realtime database
export function createUser(user, { onSuccess, onError }) {
  // return database.ref('users').child(user.uid).update({ ...user })
  //     .then(onSuccess)
  //     .catch(onError);
}

export function getAnnouncements(accessToken, { onSuccess, onError }) {}

export function getUser(tokenType, accessToken, { onSuccess, onError }) {
  DEFAULT_HEADER.headers.Authorization = tokenType + " " + accessToken;
  return axios
    .get(USER_URL, DEFAULT_HEADER)
    .then(onSuccess)
    .catch(onError);
}

export function getWallet(tokenType, accessToken, { onSuccess, onError }) {
  DEFAULT_HEADER.headers.Authorization = tokenType + " " + accessToken;
  return axios
    .get(WALLET_URL, DEFAULT_HEADER)
    .then(onSuccess)
    .catch(onError);
}

export function getTransactions(
  tokenType,
  accessToken,
  { onSuccess, onError }
) {
  DEFAULT_HEADER.headers.Authorization = tokenType + " " + accessToken;
  return axios
    .get(TRANSACTIONS_URL, DEFAULT_HEADER)
    .then(onSuccess)
    .catch(onError);
}

export function collectPoints(
  tokenType,
  accessToken,
  url,
  { onSuccess, onError }
) {
  DEFAULT_HEADER.headers.Authorization = tokenType + " " + accessToken;
  return axios
    .get(url, DEFAULT_HEADER)
    .then(onSuccess)
    .catch(onError);
}

export function getOrder(accessToken, { onSuccess, onError }) {}

export function createOrder(accessToken, { onSuccess, onError }) {}

export function retrieveOrder(accessToken, { onSuccess, onError }) {}

export function updateOrder(accessToken, { onSuccess, onError }) {}

export function getCashouts(accessToken, { onSuccess, onError }) {
  DEFAULT_HEADER.headers.Authorization = tokenType + " " + accessToken;
  return axios
    .get(CASHOUTS_URL, DEFAULT_HEADER)
    .then(onSuccess)
    .catch(onError);
}
export function createCashout(accessToken, user, data, { onSuccess, onError }) {
  DEFAULT_HEADER.headers.Authorization = tokenType + " " + accessToken;
  let body = {
    customer_id: data.customerId,
    amount: data.amount,
    status: "PENDING"
  };

  return axios
    .post(CASHOUTS_URL, body)
    .then(onSuccess)
    .catch(onError);
}

export function retrieveCashout(accessToken, { onSuccess, onError }) {}

export function updateCashout(accessToken, { onSuccess, onError }) {}
