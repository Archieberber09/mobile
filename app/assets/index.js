

const assets = {
    BACKGROUND_IMAGE    : require('./images/Coop-bg.png'),
    LOGO                : require('./images/ICONwhite.png'),
    MINI_LOGO           : require('./images/Coop-logo-mini.png'),
    AVATAR              : require('./images/Avatar.png'),
    WITHDRAWAL          : require('./images/withdrawal.png'),

};



export default assets;