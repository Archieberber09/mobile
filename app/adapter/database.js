import {createUser, getWallet, getUser, getTransactions, collectPoints} from "../api/coop_api";
import Transaction from '../model/Transaction'


export async function mcreateUser(user, data){
    let onSuccess = (user) => {
        console.log("onsuccess: "+user);

        /*
        CoopUSer is an implementation of User interface
        both should have the same number and name of methods
        new CoopUser() // constructor or other method should parse the api data
        CoopUser must be done in the api file, not here. ...might
         */

        return user
    };

    let onError = (error) => {console.log("onerror: "+error);return Promise.reject(error.message)};


    /*
    new CoopUser(data) //user should be in application context

    pass as User.toJSON

    the assignment statements below should be away
     */

    // data['uid'] = user.user.uid;
    // data['password'] = null;
    // data['confirm password'] = null;

    return createUser(data,
        {onSuccess:onSuccess, onError:onError})
}

export async function mgetTransactions(user){
    let onSuccess = (transactions) => {
        console.log(transactions);

        return (transactions.data).map(transaction => new Transaction(transaction))
    };

    let onError = (error) => {console.log("onerror: "+error);return Promise.reject(error.message)};

    return getTransactions(user.getTokenType(), user.getAccessToken(),
        {onSuccess:onSuccess, onError:onError})
}

export async function mgetWallet(user){
    let onSuccess = (wallet) => {
        console.log('mgetWallet success');
        console.log(wallet);
        user.setPoints(wallet.data.points)

        return user
    };

    let onError = (error) => {console.log("onerror: "+error);return Promise.reject(error.message)};

    console.log(user)

    return getWallet(user.getTokenType(), user.getAccessToken(),
        {onSuccess:onSuccess, onError:onError})
}

export async function mgetUser(user){
    let onSuccess = (u) => {
        return user.setAttributes(u.data)
    };

    let onError = (error) => {console.log("onerror: "+error);return Promise.reject(error.message)};

    console.log(user)

    return getUser(user.getTokenType(), user.getAccessToken(),
        {onSuccess:onSuccess, onError:onError})
}


export async function mCollectPoints(user, url){
    let onSuccess = (o) => {
        user.setPoints(o.data.wallet.points)
        let order = (o.data.order) ? new Transaction(o.data.order) : null

        console.log("SUCCESS")
        return [user, order]
    };

    let onError = (error) => {console.log("onerror: "+error);return Promise.reject(error.message)};

    console.log("collecting points")

    return collectPoints(user.getTokenType(), user.getAccessToken(), url,
        {onSuccess:onSuccess, onError:onError})
}