//adapter is responsible for the type of data it
// receives and sends

import {login, logout, register, resetPassword, sendEmailVerification} from "../api/coop_api";
import User from '../model/User'

export async function mlogin(data){

    let onSuccess   =(user)=>{return new User(user.data)};

    let onError     =(error)=> Promise.reject(error.message);

    return login(data.email, data.password,
        {onSuccess:onSuccess, onError:onError})
}

export async function mlogout(data){

    let onSuccess = ()=> {};

    let onError = (error) => Promise.reject(error.message);

    return logout(data.token_type, data.access_token,
        {onSuccess:onSuccess, onError:onError})
}

export async function mregister(data){
    let onSuccess  = (user) => {console.log("onsuccessmreg: "+user);return user};

    let onError = (error) =>  {console.log("onsuccessmreg: "+error);return Promise.reject(error.message)};

    return register(data['full name'], data.email, data.password, data['phone number'], {onSuccess:onSuccess, onError:onError})
}

export async function msendEmailVerification(){
    let onSuccess = (user) => user;

    let onError = (error) => Promise.reject(error.message);

    return sendEmailVerification(
        {onSuccess:onSuccess, onError:onError})
}

export async function mresetPassword(data){
    let onSuccess = (user) => user;

    let onError = (error) => Promise.reject(error.message);

    return resetPassword(data.email,
        {onSuccess:onSuccess, onError:onError});
}